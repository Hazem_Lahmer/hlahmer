<?php
    
    function chargerClasse($classe)
    {
        require_once $classe . '.php'; // On inclut la classe correspondante au paramètre passé.
    }

    spl_autoload_register('chargerClasse');

    $p = new Plateau ('SAHAR', 'HAZEM');

    echo $p . '<br />';

    $monstre1 = new Monstre(3,4,1) ;
    $monstre2 = new Monstre(4,8,2) ;
    $monstre3 = new Monstre(5,7,5) ;
    $monstre4 = new Monstre(5,5,9) ;
    $monstre5 = new Monstre(9,2,2) ;

    $monstre6 = new Monstre(3,8,3) ;
    $monstre7 = new Monstre(1,1,6) ;
    $monstre8 = new Monstre(5,3,2) ;
    $monstre9 = new Monstre(6,7,7) ;
    $monstre10 = new Monstre(3,3,8) ;

    $p->joueurA()->placerMonstre($monstre1);
    $p->joueurA()->placerMonstre($monstre2);
    $p->joueurA()->placerMonstre($monstre3);
    $p->joueurA()->placerMonstre($monstre4);
    $p->joueurA()->placerMonstre($monstre5);

    $p->joueurB()->placerMonstre($monstre6);
    $p->joueurB()->placerMonstre($monstre7);
    $p->joueurB()->placerMonstre($monstre8);
    $p->joueurB()->placerMonstre($monstre9);
    $p->joueurB()->placerMonstre($monstre10);

    for($i=1; $i<=$p->joueurA()->nombreMonstres(); $i++)
    {
        echo ('<p>Monstre num ' . $i . ' du joueur A<p>') ;        
    }
    
    echo '<br />';

    for($i=1; $i<=$p->joueurB()->nombreMonstres(); $i++)
    {
        echo ('<p>Monstre num ' . $i . ' du joueur B</p>') ;        
    }

?>