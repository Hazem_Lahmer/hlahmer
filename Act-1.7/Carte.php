<?php
    
    class Carte
    {
        protected $_mana, $_degats;
        
        // initialisation des données
        public function __construct($degats, $mana)
        {
          $this->setDegats($degats);
          $this->setMana($mana);
        }
        
        // GETTERS
        public function degats()
        {
            return $this->_degats;
        }
        
        public function mana()
        {
            return $this->_mana;
        }
        

        // SETTERS
        public function setDegats($degats)
        {
            $degats = (int) $degats;
            
            if ($degats >= 0)
            {
            $this->_degats = $degats;
            }
        }
        
        public function setMana($mana)
        {
            $mana = (int) $mana;
            
            if ($mana > 0)
            {
            $this->_mana = $mana;
            }
        }
    }

?>