<?php

    require_once("Carte.php");

    class Sort extends Carte
    {
        //Une fonction « attaquer » qui prendra en paramètre une instance de Monstre et qui lui retirera des points de vie correspondants au nombre de points de dégâts de l’attaquant
        public function attaquer(Monstre $monstre)
        {
            if ($monstre->_vie >= $this->_degats)
            {
                $monstre->_vie -= $this->_degats;
                echo 'Le monstre a bien été frappé !';
            }
            else
            {
                $monstre->_vie = 0;
                echo 'Vous avez tué ce monstre !';
            }  
        }
    }

?>