<?php
    
    require_once("Joueur.php");
    
    class Plateau
    {
        private static $_nbMonstreParJoueur;
        private $_joueurA, $_joueurB ;
        
        // initialisation des données
        public function __construct($joueurA, $joueurB, $nbMonstreParJoueur=5)
        {
            static::setNbMonstreParJoueur($nbMonstreParJoueur);
            $this->setJoueurs($joueurA, $joueurB);
        }

        // La fonction pour afficher le plateau dans main
        public function __toString()
        {
            return "<p>Nombre de monstres par personne : " . static::$_nbMonstreParJoueur . "</p><p>nom du joueur A: " . $this->_joueurA->pseudo() . "</p><p>nom du joueur B: " . $this->_joueurB->pseudo() . '</p>' ;
        }

        // GETTERS
        public function nbMonstreParJoueur()
        {
            return $this->_nbMonstreParJoueur;
        }
        
        public function joueurA()
        {
            return $this->_joueurA;
        }
        
        public function joueurB()
        {
            return $this->_joueurB;
        }

        // SETTERS
        public function setNbMonstreParJoueur($nbMonstreParJoueur)
        {
            $nbMonstreParJoueur = (int) $nbMonstreParJoueur;
            
            if ($nbMonstreParJoueur > 0)
            {
                static::$_nbMonstreParJoueur = $nbMonstreParJoueur;
            }
        }
        
        public function setJoueurs($joueurA, $joueurB)
        {            
            if ((is_string($joueurA)) && (is_string($joueurB)))
            {
                $this->_joueurA = new Joueur ($joueurA, static::$_nbMonstreParJoueur);
                $this->_joueurB = new Joueur ($joueurB, static::$_nbMonstreParJoueur);
            }
        }
    }

?>