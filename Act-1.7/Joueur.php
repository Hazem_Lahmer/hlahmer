<?php

    require_once("Monstre.php");

    class Joueur
    {
        private $_nbMonstre, $_mana, $_vie, $_pseudo, $_monstresPlace ;
        
        // initialisation des données
        public function __construct($pseudo, $nbMonstreParJoueur)
        {
            $this->_monstresPlace = array();
            $this->setPseudo($pseudo);
            $this->setNbMonstre($nbMonstreParJoueur);
            $this->_vie = 30;
            $this->_mana = 10;
        }
        
        // Pour récupérer le nombre des monstres disponibles
        public function nombreMonstres()
        {
            return count($this->_monstresPlace);
        }

        // une fonction pour ajouter un monstre dans monstrePlace
        public function placerMonstre (Monstre $monstre)
        {
            if (count($this->_monstresPlace) < $this->_nbMonstre)
            {
                array_push($this->_monstresPlace, $monstre);
            }
        }

        // GETTERS
        public function monstresPlace()
        {
            return $this->_monstresPlace;
        }

        public function pseudo()
        {
            return $this->_pseudo;
        }
        
        public function vie()
        {
            return $this->_vie;
        }
        
        public function mana()
        {
            return $this->_mana;
        }

        // SETTERS
        public function setPseudo($pseudo)
        {
          if (is_string($pseudo))
          {
            $this->_pseudo = $pseudo;
          }
        }

        public function setNbMonstre($nbMonstreParJoueur)
        {
            $nbMonstreParJoueur = (int) $nbMonstreParJoueur;
            
            if ($nbMonstreParJoueur > 0)
            {
                $this->_nbMonstre = $nbMonstreParJoueur;
            }
        }
    }

?>