<?php
    
    require_once("Carte.php");

    class Monstre extends Carte implements DamageableInterface
    {
        private $_vie ;
        
        // initialisation des données
        public function __construct($degats, $vie, $mana)
        {
          $this->setDegats($degats);
          $this->setVie($vie);
          $this->setMana($mana);
        }
        
        // Définition de la méthode implémenté de l'interface
        public function takeDamages(int $n)
        {
            if ($this->_vie >= $n)
            {
                $this->_vie -= $n ;
                echo 'Vous avez frappé ce monstre !';
            }
            else
            {
                $this->_vie = 0;
                echo 'Vous avez tué ce monstre !';
            }
        }

        //Une fonction « attaquer » qui prendra un paramètre de type damageableInterface et qui lui retirera des points de vie correspondants au nombre de points de dégâts de l’attaquant
        public function attaquer(DamageableInterface $DamageableInterface)
        {
            $DamageableInterface->takeDamages($this->_degats);       
        }

        // GETTERS      
        public function vie()
        {
            return $this->_vie;
        }      

        // SETTERS       
        public function setVie($vie)
        {
            $vie = (int) $vie;
            
            if ($vie > 0)
            {
            $this->_vie = $vie;
            }
        }        
    }

?>