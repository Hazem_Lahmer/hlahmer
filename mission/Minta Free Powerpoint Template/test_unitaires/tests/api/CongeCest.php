<?php namespace App\Tests;

use App\tests\ApiTester;

class CongeCest
{
    public function _before(ApiTester $apiTester)
    {
        $apiTester->connectApi();
    }

    /**
     * @param ApiTester $apiTester
     */
    public function getConges(ApiTester $apiTester)
    {
        $apiTester->wantTo('get all conges');
        $apiTester->sendGET('api/conges');
        $apiTester->seeResponseCodeIs(200);
    }

    /**
     * @param ApiTester $apiTester
     */
    public function getConge(ApiTester $apiTester)
    {
        $apiTester->wantTo('get an only conge');
        $apiTester->sendGET('api/conges/1');
        $apiTester->seeResponseCodeIs(200);
    }


    /**
     * @param \App\Tests\ApiTester $apiTester
     */
    public function addConge(\App\Tests\ApiTester $apiTester)
    {
        $apiTester->wantTo('POST conges');
        $apiTester->sendPost('api/conges',
            ['dateDebut' => '2020-03-03',
                'nombreJours' => '3',
            ]);
        $apiTester->seeResponseCodeIs(201);

    }

    /**
     * @param \App\Tests\ApiTester $apiTester
     */
    public function putConge(\App\Tests\ApiTester $apiTester)
    {
        $apiTester->wantTo('PUT conges');
        $apiTester->sendPUT('api/conges/1',
            ['dateDebut' => '2020-03-03',
                'nombreJours' => '4',
            ]);
        $apiTester->seeResponseCodeIs(200);
    }

    /**
     * @param \App\Tests\ApiTester $apiTester
     */
    public function deleteConge(\App\Tests\ApiTester $apiTester)
    {
        $apiTester->wantTo('delete conges');
        $apiTester->sendDELETE('api/conges/1');
        $apiTester->seeResponseCodeIs(204);
    }


}
