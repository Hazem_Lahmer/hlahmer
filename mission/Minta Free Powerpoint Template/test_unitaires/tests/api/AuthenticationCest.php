<?php namespace App\Tests;
use App\Tests\ApiTester;

class AuthentificationCest
{
    private $username;
    private $password;

    public function _before()
    {
        $config = \Codeception\Configuration::config();
        $apiSettings = \Codeception\Configuration::suiteSettings('api', $config);
        $this->username = $apiSettings['params']['username'];
        $this->password = $apiSettings['params']['password'];
    }

    /**
     * @param \App\Tests\ApiTester $apiTester
     */
    public function authenticationSuccessTest(ApiTester $apiTester)
    {
        $apiTester->wantTo('authenticate a user successfully and recieve a token');
        $apiTester->haveHttpHeader('Content-Type', 'application/json');
        $apiTester->sendPOST('api/login_check', [
            'username' => $this->username,
            'password' => $this->password
        ]);
        $apiTester->seeResponseCodeIs(200);
    }
}
