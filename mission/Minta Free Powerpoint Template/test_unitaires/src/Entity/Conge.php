<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;


/**
 * @ORM\Entity(repositoryClass="App\Repository\CongesRepository")
 */
class Conges
{
    public function __construct($user)
    {
        $this->setUser($user);
    }
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="conges")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Serializer\Groups({"users"})
     */
    private $user;

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"conges"})
     */
    private $id;


    /**
     * @ORM\Column(type="date")
     * @Serializer\Groups({"conges"})
     */
    private $date_debut;

    /**
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"conges"})
     */
    private $nombre_jours;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->date_debut;
    }

    public function setDateDebut(\DateTimeInterface $date_debut): self
    {
        $this->date_debut = $date_debut;

        return $this;
    }

    public function getNombreJours(): ?int
    {
        return $this->nombre_jours;
    }

    public function setNombreJours(int $nombre_jours): self
    {
        $this->nombre_jours = $nombre_jours;

        return $this;
    }
}
