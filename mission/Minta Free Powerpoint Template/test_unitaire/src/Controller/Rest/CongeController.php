<?php

namespace App\Controller\Rest;

use App\Entity\Conges;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;



/**
 * Class CongeController
 * @package App\Controller\Rest
 * @Route("/api/conges", name="conges")
 */
class CongeController extends  AbstractFOSRestController
{
    protected $em;
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Retrieves a collection of Conge resource
     * @Rest\Get("/")
     * @Rest\View(serializerGroups={"conges"})
     */
    public function getConges() : View
    {
        $conges = $this->em->getRepository(Conges::class)->findAll();

        // In case our GET was a success we need to return a 200 HTTP OK response with the collection of article object
        return View::create($conges, Response::HTTP_OK);
    }

    /**
     * Retrieves an Conge resource
     * @Rest\Get("/{id}")
     * @Rest\View(serializerGroups={"conges"})
     */
    public function getCongesById(int $id): View
    {
        $article = $this->em->getRepository(Conges::class)->findOneBy(['id' => $id]);

        // In case our GET was a success we need to return a 200 HTTP OK response with the request object
        return View::create($article, Response::HTTP_OK);
    }

    /**
     * Creates a Conge resource
     * @Rest\Post("")
     * @Rest\View(serializerGroups={"conges"})
     * @param Request $request
     * @return View
     * @throws
     */
    public function postConges(Request $request) : View
    {
        //$this->denyAccessUnlessGranted('ROLE_USER');
        $userConnected = $this->getUser();

        $conge = new Conges($userConnected);

        $params = $request->request->all();

        $dateDebut = new \DateTime($params['dateDebut']);
        $conge->setDateDebut($dateDebut);
        $conge->setNombreJours($params['nombreJours']);

        $this->em->persist($conge);
        $this->em->flush();

        // In case our POST was a success we need to return a 201 HTTP CREATED response
        return View::create($conge, Response::HTTP_CREATED);
    }

    /**
     * Replaces Conge resource
     * @Rest\Put("/{id}")
     * @Rest\View(serializerGroups={"conges"})
     * @throws \Exception
     */
    public function putConge(int $id, Request $request): View
    {
        $conge = $this->em->getRepository(Conges::class)->findOneBy(['id'=>$id]);

        if($conge){
            $params = $request->request->all();

            $dateDebut = new \DateTime($params['dateDebut']);
            $conge->setDateDebut($dateDebut);
            $conge->setNombreJours($params['nombreJours']);

            $this->em->persist($conge);
            $this->em->flush();
        }
        // In case our PUT was a success we need to return a 200 HTTP OK response with the object as a result of PUT
        return View::create($conge, Response::HTTP_OK);
    }

    /**
     * Removes the Article resource
     * @Rest\Delete("/{id}")
     * @Rest\View(serializerGroups={"conges"})
     */
    public function deleteConge(int $id): View
    {
        $conge = $this->em->getRepository(Conges::class)->findOneBy(['id'=>$id]);
        if ($conge) {
            $this->em->remove($conge);
            $this->em->flush();
        }
        // In case our DELETE was a success we need to return a 204 HTTP NO CONTENT response. The object is deleted.
        return View::create([], Response::HTTP_NO_CONTENT);
    }

}