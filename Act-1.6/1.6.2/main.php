<?php
    
    function chargerClasse($classe)
    {
        require $classe . '.php'; // On inclut la classe correspondante au paramètre passé.
    }

    spl_autoload_register('chargerClasse');

    //instanciation
    $poubelle1= new Poubelle('Poubelle1');
    $poubelle2= new Poubelle('Poubelle2', 8482.6426);
    $poubelle3= new Poubelle('Poubelle3', 6524.43583, 'poubelle avec tous les attributs');

    $ballais1= new Ballais('Poubelle1');
    $ballais2= new Ballais('Poubelle2', 8482.6426);
    $ballais3= new Ballais('Poubelle3', 6524.43583, 'ballais avec tous les attributs');

    echo "Poubelle1<br />";
    $poubelle1->consulter_argent($poubelle1->prix()); echo "<br />";
    $poubelle1->consulter_fiche(); echo "<br />";
    echo "-----------------------------<br />";

    echo "Poubelle2<br />";
    $poubelle2->consulter_argent($poubelle2->prix()); echo "<br />";
    $poubelle2->consulter_fiche(); echo "<br />";
    echo "-----------------------------<br />";

    echo "Poubelle3<br />";
    $poubelle3->consulter_argent($poubelle3->prix()); echo "<br />";
    $poubelle3->consulter_fiche(); echo "<br />";
    echo "-----------------------------<br />";

    echo "-----------------------------<br />";

    echo "Ballais1<br />";
    $ballais1->consulter_argent($ballais1->prix()); echo "<br />";
    $ballais1->consulter_fiche(); echo "<br />";
    echo "-----------------------------<br />";

    echo "Ballais2<br />";
    $ballais2->consulter_argent($ballais2->prix()); echo "<br />";
    $ballais2->consulter_fiche(); echo "<br />";
    echo "-----------------------------<br />";

    echo "Ballais3<br />";
    $ballais3->consulter_argent($ballais3->prix()); echo "<br />";
    $ballais3->consulter_fiche(); echo "<br />";
    echo "-----------------------------<br /><br />";

    echo "<strong> La classe Produit est une classe abstraite.</strong>"



?>