<?php

    require_once("../1.6.3/Consulter.php");

    abstract class Produit
    {
        protected $_prix;
        protected $_nom;
        protected $_description;
        protected static $_code;

        use Consulter;
        
        public function __construct($nom,$prix=null,$description=null)
        {
            $this->setPrix($prix);
            $this->setNom($nom);
            $this->setDescription($description);
            static::$_code += 1;
        }
    
        public function consulter_fiche()
        {
            if (isset ($this->_nom))
            {
                echo 'le nom du produit : '. $this->_nom.'<br />';
            }
            
            if (isset ($this->_prix))
            {
                echo 'le prix du produit : ';
                $this->consulter_argent($this->_prix);
            }

            if (isset ($this->_description))
            {
                echo 'description : '. $this->_description.'<br />';
            } 
        }
        
        // GETTERS //

        public function prix()
        {
        return $this->_prix;
        }
        
        public function nom()
        {
        return $this->_nom;
        }

        public function description()
        {
        return $this->_description;
        }

        public function code()
        {
        return $this->_code;
        }

        // SETTERS //

        public function setPrix($prix)
        {
            if ($prix > 0)
            {
                $this->_prix = $prix;
            }
        }

        public function setNom($nom)
        {
            if (is_string($nom))
            {
                $this->_nom = $nom;
            }
        }

        public function setDescription($description)
        {
            if (is_string($description))
            {
                $this->_description = $description;
            }
        }
    } 

?>