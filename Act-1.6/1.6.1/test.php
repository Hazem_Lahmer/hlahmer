<?php
    
    function chargerClasse($classe)
    {
        require $classe . '.php'; // On inclut la classe correspondante au paramètre passé.
    }

    spl_autoload_register('chargerClasse');


    //instanciation
    $compte_ordinaire= new Compte(8425639.14586);
    $compte_epargne= new CompteEpargne(135616516541.9597);
    $compte_payant= new ComptePayant(95613453.464865);

    //On affiche notre solde
    $compte_ordinaire->consulter_argent($compte_ordinaire->solde()); echo "<br />";
    $compte_epargne->consulter_argent($compte_epargne->solde()); echo "<br />";
    $compte_payant->consulter_argent($compte_payant->solde()); echo "<br />";

    echo "-----------------------------<br />" ;

    //depot du money dans chaque instance
    $compte_ordinaire->deposer(10);
    $compte_epargne->deposer(10);
    $compte_payant->deposer(10);

    //On affiche notre solde
    $compte_ordinaire->consulter_argent($compte_ordinaire->solde()); echo "<br />";
    $compte_epargne->consulter_argent($compte_epargne->solde()); echo "<br />";
    $compte_payant->consulter_argent($compte_payant->solde()); echo "<br />";

    echo "-----------------------------<br />" ;

    //retrait du money dans chaque instance
    $compte_ordinaire->retirer(5);
    $compte_epargne->retirer(5);
    $compte_payant->retirer(5);

    //On affiche notre solde
    $compte_ordinaire->consulter_argent($compte_ordinaire->solde()); echo "<br />";
    $compte_epargne->consulter_argent($compte_epargne->solde()); echo "<br />";
    $compte_payant->consulter_argent($compte_payant->solde()); echo "<br />";

    echo "-----------------------------<br />" ;

    $compte_epargne->calculInteret();
    $compte_epargne->consulter_argent($compte_epargne->solde()); echo "<br />";

    echo "<strong> La classe Compte est une classe.</strong>"

?>