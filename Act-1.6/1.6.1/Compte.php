<?php

    require_once("../1.6.3/Consulter.php");

    class Compte
    {
        protected $_solde;
        protected static $_code;

        use Consulter ;
        
        public function __construct($solde=null)
        {
            $this->_solde = $solde;
            static::$_code += 1;
        }

        public function deposer($montant)
        {
            if ($montant > 0)
            {
            $this->_solde += $montant;
            }   
        }

        public function retirer($montant)
        {
            if (($montant > 0) && ($montant < $this->_solde))
            {
            $this->_solde -= $montant;   
            }   
        }

        // GETTERS //

        public function solde()
        {
        return $this->_solde;
        }

        public function code()
        {
        return $this->_code;
        }
    }

?>