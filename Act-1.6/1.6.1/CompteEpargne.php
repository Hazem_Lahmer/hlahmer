<?php

    require_once("Compte.php");

    class CompteEpargne extends Compte
    {
        protected $_interet=6;

        public function calculInteret()
        {
                $this->_solde = (($this->_interet*$this->_solde)/100) + $this->_solde;
        }
    }

?>