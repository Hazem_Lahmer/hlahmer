﻿commande à executer :

composer install
php bin/console doctrine:database:create
bin/console doctrine:schema:update --force
php bin/console s:r


La BDD :  "projethazem"
deux tables : first_form("id","nom","prenom","age","adresse","code_postal","ville","permis") / second_form("id","montant","email","telephone") / second_form("id","email")


activité 1

fichiers concernés : firstform.html.twig , Entity/FirstForm.php , Controller/FirstFormController.php , From/FirstFormType.php

Route à executer : http://127.0.0.1:8000/premierformulaire

------------------------------------------------------------------------------------------------------------------------------

activité 2

fichiers concernés : secondform.html.twig , Entity/SecondForm.php , Controller/SecondFormController.php , From/SecondFormType.php

Pour y accéder : 

	- Route à executer : http://127.0.0.1:8000/secondformulaire

	- En "submittant" le premier formulaire

------------------------------------------------------------------------------------------------------------------------------

activité 3

fichiers concernés : thirdform.html.twig , Entity/ThirdForm.php , Controller/ThirdFormController.php , From/ThirdFormType.php

Route à executer : http://127.0.0.1:8000/troisiemeformulaire

------------------------------------------------------------------------------------------------------------------------------

activité 4

fichiers concernés : thirdform.html.twig , Entity/ThirdForm.php , Controller/ThirdFormController.php , From/ThirdFormType.php

Route à executer : http://127.0.0.1:8000/troisiemeformulaire

------------------------------------------------------------------------------------------------------------------------------

activité 5

fichiers concernés : equipe.html.twig , Entity/Equipe.php , Entity/Joueur.php, Controller/EquipeController.php , From/EquipeType.php

Route à executer : http://127.0.0.1:8000/equipeform

------------------------------------------------------------------------------------------------------------------------------


