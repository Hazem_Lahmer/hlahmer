<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ThirdForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\ThirdFormType;
use Doctrine\ORM\EntityManagerInterface;

class ThirdFormController extends Controller
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/troisiemeformulaire", name="thirdform")
     */
    public function formAction(Request $request)
    {
        $thirdForm = new ThirdForm();

        $form = $this->createForm(ThirdFormType::class, $thirdForm);

        // Si la requête est en POST
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid() && $form->isSubmitted()) {
                $thirdForm = $form->getData();
                $this->entityManager->persist($thirdForm);
                $this->entityManager->flush();

                $this->addFlash('success', 'troisième formulaire enregisré');

                // On redirige vers la page de visualisation de l'annonce nouvellement créée
                return $this->redirectToRoute('thirdform');
            }
        }

        // On passe la méthode createView() du formulaire à la vue
        // afin qu'elle puisse afficher le formulaire toute seule
        return $this->render('formulaire/thirdform.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
