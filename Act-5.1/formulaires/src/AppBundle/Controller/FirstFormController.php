<?php

namespace AppBundle\Controller;

use AppBundle\Entity\FirstForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\FirstFormType;
use Doctrine\ORM\EntityManagerInterface;


class FirstFormController extends Controller
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/premierformulaire", name="firstform")
     */
    public function formAction(Request $request)
    {
        $firstForm = new FirstForm();

        $form = $this->createForm(FirstFormType::class, $firstForm);

        // Si la requête est en POST
    if ($request->isMethod('POST')) 
    {
        $form->handleRequest($request);

        
        if ($form->isValid() && $form->isSubmitted()) 
        {
            $firstForm = $form->getData();
            $this->entityManager->persist($firstForm);
            $this->entityManager->flush();

            $this->addFlash('success', 'Premier formulaire enregisré');

            // On redirige vers la page de visualisation de l'annonce nouvellement créée
            return $this->redirectToRoute('secondform');
        }
    }

        // On passe la méthode createView() du formulaire à la vue
        // afin qu'elle puisse afficher le formulaire toute seule
        return $this->render('formulaire/firstform.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
