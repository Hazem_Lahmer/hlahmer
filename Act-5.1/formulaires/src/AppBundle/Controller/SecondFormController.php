<?php

namespace AppBundle\Controller;

use AppBundle\Entity\SecondForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\SecondFormType;
use Doctrine\ORM\EntityManagerInterface;

class SecondFormController extends Controller
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/secondformulaire", name="secondform")
     */
    public function formAction(Request $request)
    {
        $secondForm = new SecondForm();

        $form = $this->createForm(SecondFormType::class, $secondForm);

        // Si la requête est en POST
    if ($request->isMethod('POST')) 
    {
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) 
        {
            $secondForm = $form->getData();
            $this->entityManager->persist($secondForm);
            $this->entityManager->flush();

            $this->addFlash('success', 'Second formulaire enregisré');

            // On redirige vers la page de visualisation de l'annonce nouvellement créée
            return $this->redirectToRoute('secondform');
        }
    }

        // On passe la méthode createView() du formulaire à la vue
        // afin qu'elle puisse afficher le formulaire toute seule
        return $this->render('formulaire/secondform.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
