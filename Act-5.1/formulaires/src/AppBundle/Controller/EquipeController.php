<?php

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Equipe;
use AppBundle\Entity\Joueur;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\EquipeType;
use AppBundle\Form\JoueurType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraints\Length;

class EquipeController extends Controller
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/equipeform", name="equipeform")
     */
    public function formAction(Request $request)
    {
        $equipe = new Equipe();
        $form = $this->createForm(EquipeType::class, $equipe);

        // Si la requête est en POST
    if ($request->isMethod('POST')) 
    {
        $form->handleRequest($request);
        
        if ($form->isValid() && $form->isSubmitted()) 
        {
            $equipe = $form->getData();
            $this->entityManager->persist($equipe);

            $this->entityManager->flush();

            $this->addFlash('success', 'Le formulaire de l\'équipe est enregisré');
            
            // On redirige vers la page de visualisation de l'annonce nouvellement créée
            return $this->redirectToRoute('equipeform');
        }
    }

        // On passe la méthode createView() du formulaire à la vue
        // afin qu'elle puisse afficher le formulaire toute seule
        return $this->render('formulaire/equipe.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
