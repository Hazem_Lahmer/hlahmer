<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * FirstForm
 *
 * @ORM\Table(name="first_form")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FirstFormRepository")
 */
class FirstForm
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * 
     * @Assert\NotBlank(message = "Veillez saisir votre nom")
     * 
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     * 
     * @Assert\NotBlank(message = "Veillez saisir votre prénom")
     * 
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var int
     * 
     * @Assert\GreaterThan(1, message="Veuillez introduire un age supérieur à 1")
     * 
     * @Assert\LessThan(100, message="Veuillez introduire un age inférieur à 100")
     * 
     * @ORM\Column(name="age", type="integer", nullable=true, options={"default" : NULL})
     */
    private $age;

    /**
     * @var string
     * 
     * @Assert\NotBlank(message = "Veillez saisir votre adresse")
     * 
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "Votre adresse ne peut pas dépasser {{ limit }} characters"
     * )
     * 
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    private $adresse;

    /**
     * @var int
     * 
     * @Assert\NotBlank(message = "Veillez saisir votre code postal")
     * 
     * @Assert\LessThan(100000, message="Veuillez introduire un code postal à 5 chiffres")
     * 
     * @Assert\GreaterThan(9999, message="Veuillez introduire un code postal à 5 chiffres")
     * 
     * @ORM\Column(name="code_postal", type="integer")
     */
    private $codePostal;

    /**
     * @var string
     * 
     * @Assert\NotBlank(message = "Veillez saisir le nom de votre ville")
     * 
     * @ORM\Column(name="ville", type="string", length=255)
     */
    private $ville;

    /**
     * @var array
     * 
     * @ORM\Column(name="permis", type="array", length=255, nullable=true, options={"default" : "NULL"})
     */
    private $permis;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return FirstForm
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return FirstForm
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set age
     *
     * @param integer $age
     *
     * @return FirstForm
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age
     *
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return FirstForm
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set codePostal
     *
     * @param integer $codePostal
     *
     * @return FirstForm
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return int
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return FirstForm
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set permis
     *
     * @param string $permis
     *
     * @return FirstForm
     */
    public function setPermis($permis)
    {
        $this->permis = $permis;

        return $this;
    }

    /**
     * Get permis
     *
     * @return string
     */
    public function getPermis()
    {
        $permis = $this->permis;
        if ($permis != NULL) 
        {
            return explode(" ", $permis);
        } else 
        {
            return $this->permis;
        }
    }
}
