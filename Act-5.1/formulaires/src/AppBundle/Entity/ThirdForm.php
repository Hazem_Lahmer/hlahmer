<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ThirdForm
 *
 * @ORM\Table(name="third_form")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ThirdFormRepository")
 */
class ThirdForm
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * 
     * @Assert\NotBlank(message = "Veillez saisir votre adresse mail")
     * 
     * @Assert\Regex(pattern="/^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/",
     * message="Votre adresse mail devra être sous la forme x@xx.xx")
     * 
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     * 
     * @Assert\NotBlank(message = "Veillez saisir un url d'une annonce du boncoin")
     * 
     * @Assert\Regex(pattern="/^https:\/\/w{3}\.leboncoin\.fr\/[a-zA-Z]+\/[a-zA-Z0-9]{10}\.htm\/$/",
     * message="Votre URL devra être sous la forme https://www.leboncoin.fr/categorie/identifiant.htm/")
     * 
     * @ORM\Column(name="affaire_leboncoin", type="string", length=255)
     */
    private $affaire_leboncoin;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return ThirdForm
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set affaire_leboncoin
     *
     * @param string $affaire_leboncoin
     *
     * @return ThirdForm
     */
    public function setAffaireLeboncoin($affaire_leboncoin)
    {
        $this->affaire_leboncoin = $affaire_leboncoin;

        return $this;
    }

    /**
     * Get affaire_leboncoin
     *
     * @return string
     */
    public function getAffaireLeboncoin()
    {
        return $this->affaire_leboncoin;
    }
}

