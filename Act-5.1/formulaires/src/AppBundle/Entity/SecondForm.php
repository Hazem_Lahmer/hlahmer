<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * SecondForm
 *
 * @ORM\Table(name="second_form")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SecondFormRepository")
 */
class SecondForm
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(name="montant", type="decimal", precision=10, scale=0)
     */
    private $montant;

    /**
     * @var string
     * 
     * @Assert\NotBlank(message = "Veillez saisir votre adresse mail")
     * 
     * @Assert\Regex(pattern="/^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/",
     * message="Votre adresse mail devra être sous la forme x@xx.xx")
     * 
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     * 
     * @Assert\NotBlank(message = "Veillez saisir votre numéro de téléphone")
     * 
     * @Assert\Regex(pattern="/^(0)(6|7)[0-9]{8}$/",
     * message="Your téléphone must contain 10 numbers and start with 06 or 07")
     * 
     * @ORM\Column(name="telephone", type="string")
     */
    private $telephone;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set montant
     *
     * @param string $montant
     *
     * @return SecondForm
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return string
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return SecondForm
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telephone
     *
     * @param integer $telephone
     *
     * @return SecondForm
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return int
     */
    public function getTelephone()
    {
        return $this->telephone;
    }
}

