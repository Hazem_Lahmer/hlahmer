<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class FirstFormType extends AbstractType
{
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder->add('nom', TextType::class)
        ->add('prenom', TextType::class)
        ->add('age', IntegerType::class, ['attr' => ['min' => 1, 'max' => 100]], ['required' => false])
        ->add('adresse', TextareaType::class)
        ->add('codePostal', IntegerType::class, ['attr' => ['min' => 10000, 'max' => 99999]])
        ->add('ville', ChoiceType::class, ['choices'  => ['Deutschland' => 'Deutschland', 'Tunisia' => 'Tunisia', 'Japan' => 'Japan']])
        ->add('permis', ChoiceType::class, ['choices' => ['AM'=>'AM', 'A1'=>'A1', 'A2'=>'A2', 'A'=>'A', 'B'=>'B', 'B1'=>'B1'],
            'expanded' => 'true',
            'required' => false,
            'multiple' => true
            ])
        ->add('save', SubmitType::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\FirstForm'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_firstform';
    }
}
