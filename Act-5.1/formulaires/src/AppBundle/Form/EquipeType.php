<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use AppBundle\Form\JoueurType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class EquipeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom_equipe', TextType::class)
            ->add('ville', TextType::class)
            ->add('sport', ChoiceType::class, [
                'choices'  => [
                    'football' => 'football',
                    'rugby' => 'rugby',
                    'handball' => 'handball',
                    'volleyball' => 'volleyball'
                ]
            ])

            /*
            * Rappel :
            ** - 1er argument : nom du champ, ici « joueurs », car c'est le nom de l'attribut
            ** - 2e argument : type du champ, ici « CollectionType » qui est une liste de quelque chose
            ** - 3e argument : tableau d'options du champ
            */
            ->add('joueurs', CollectionType::class, array(
                'entry_type'   => JoueurType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'by_reference' => false,
            ))

            ->add('save', SubmitType::class);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Equipe'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_equipe';
    }
}
