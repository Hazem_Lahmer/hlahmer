﻿commande à executer :

composer install
php bin/console s:r

------------------------------

activité 1

fichiers concernés : Controller/FileController.php

Route à executer : - http://127.0.0.1:8000/create/{filename} : pour créer une dossier "fsi" dans le web et le fichier dedans s'ils n'existent pas

		   - http://127.0.0.1:8000/write/{filename}/{text} : pour mettre le "text" dans le fichier "filename" s'il existe

		   - http://127.0.0.1:8000/copy/{oldname}/{newname} : pour copier le fichier "oldname" et créér une copie avec le nom "newname"

-------------------------------------------------------------------------------------------------------------------------------------------------------

activité 2

fichiers concernés : Controller/FileController.php , Service/FileSystemImproved.php

Route à executer : - http://127.0.0.1:8000/create-file/{filename} : pour créer une dossier "fsi" dans le web et le fichier dedans s'ils n'existent pas

		   - http://127.0.0.1:8000/write-in-file/{filename}/{text} : pour mettre le "text" dans le fichier "filename" s'il existe

		   - http://127.0.0.1:8000/read-file/{filename} : pour voir le contenu du fichier "filename"

		   - http://127.0.0.1:8000/delete-file/{filename} : pour supprimer le fichier "filename"

-------------------------------------------------------------------------------------------------------------------------------------------------------

activité 3

fichiers concernés : EventListener/ExceptionListener.php , Service/FileSystemImproved.php

Route à executer : - la premier chose à faire est d'ajouter cette ligne dans le constructeur du controller pour lancer une exception : "1/0;"

		   - n'importe qu'elle route dans le controlleur. par exemple : http://127.0.0.1:8000/create/{filename}

		   - Le fichier à consulter pour voir le message d'erreur est web/history (comme l'indique le message qui s'affiche dans le navigateur

---------------------------------------------------------------------------------------------------------------------------------------------------------