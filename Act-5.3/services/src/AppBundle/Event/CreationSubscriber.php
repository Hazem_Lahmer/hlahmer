<?php

namespace AppBundle\Event;

use symfony\Component\EventDispatcher\EventSubscriberInterface;

class CreationSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents ()
    {
        return [FileCreationEvent::MESSAGE => 'fileCreated'];
    }

    public function fileCreated ()
    {
        echo ("SUBSCRIBER : Notre fichier a été créé"); echo "\n";
    }
}