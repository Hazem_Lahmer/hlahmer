<?php

namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class FileCreationEvent extends Event
{
    public const MESSAGE = 'fichier créé';

}