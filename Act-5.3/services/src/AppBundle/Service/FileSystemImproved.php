<?php

namespace AppBundle\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class FileSystemImproved
{
    private $filesystem;

    private $files_path;

    public function __construct()
    {
        $this->filesystem = new Filesystem();
        $this->files_path = getcwd().'/fsi\/';

        if (!$this->filesystem->exists($this->files_path)) {
            $this->filesystem->mkdir($this->files_path);
        }
    }

    public function state()
    {
        $finder = new Finder();
        $finder->files()->in($this->files_path);
        $files = [];
        $i = 0;
        foreach ($finder as $file) {
            // dumps the absolute path
            $files[$i] = $file->getFileName();
            $i++;
        }

        return $files;
    }

    public function createFile($filename)
    {
        if (!$this->filesystem->exists($this->files_path . $filename)) {
            $this->filesystem->touch($this->files_path . $filename);
        }
        return $this->state();
    }

    public function deleteFile($filename)
    {
        if ($this->filesystem->exists($this->files_path . $filename)) {
            $this->filesystem->remove($this->files_path . $filename);
            return true;
        }
        return false;
    }

    public function writeInFile($filename, $text)
    {
        if ($this->filesystem->exists($this->files_path . $filename)) {
            $this->filesystem->appendToFile($this->files_path . $filename, $text);
            return true;
        }
        return false;
    }

    public function readInFile($filename)
    {
        $finder = new Finder();
        $finder->files()->in($this->files_path);
        $file = $this->files_path . $filename;

        if ($this->filesystem->exists($file)) {
            foreach ($finder as $file) {
                // dumps the absolute path
                if ($file->getFileName() == $filename) {
                    $contents = $file->getContents();
                }
            }
        } else {
            return 'fichier introuvable';
        }

        return $contents;
    }
}
