<?php

// src/AppBundle/EventListener/ExceptionListener.php
namespace AppBundle\EventListener;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use AppBundle\Service\FileSystemImproved;

class ExceptionListener
{   
    
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getException();
        $message = $exception->getMessage();

        $FileSystemImproved = new FileSystemImproved();
        $files = $FileSystemImproved->createFile('history');
        $ok = $FileSystemImproved->writeInFile('history', $message);

        // Customize your response object to display the exception details
        $response = new Response();
        $response->setContent('Le message d\'erreur est enregistré dans le fichier web/history');

        //$event->allowCustomResponseCode();
        //$response = new Response('No Content', 204);
        //$event->setResponse($response);

        

        // sends the modified response object to the event
        $event->setResponse($response);
    }

}