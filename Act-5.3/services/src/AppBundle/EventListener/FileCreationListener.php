<?php

// src/AppBundle/EventListener/ExceptionListener.php
namespace AppBundle\EventListener;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use AppBundle\Service\FileSystemImproved;

class FileCreationListener
{
    public function fileCreated ()
    {
        echo ("LISTENER : Le fichier a été créé"); echo "\n";
    }
}