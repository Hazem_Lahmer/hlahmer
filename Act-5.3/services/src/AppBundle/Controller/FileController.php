<?php

namespace AppBundle\Controller;

use AppBundle\Event\FileCreationEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Service\FileSystemImproved;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\EventListener\ExceptionListener;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class FileController extends Controller
{
    private $filesystem;
    private $FileSystemImproved;
    private $files_path;
    
        
    public function __construct(Filesystem $filesystem, FileSystemImproved $FileSystemImproved)
    {
        $this->files_path = getcwd().'/fsi\/';
        $this->filesystem = $filesystem;
        $this->FileSystemImproved = $FileSystemImproved;
    
    }
    
    
    /**
     * @Route("/create/{filename}")
     */
    public function createAction($filename)
    {
        if (!$this->filesystem->exists($this->files_path))
        {
            $this->filesystem->mkdir($this->files_path);
        }
        
        if (!$this->filesystem->exists($this->files_path.$filename))
        {
            $this->filesystem->touch($this->files_path .$filename);
        }
        else
        {
            return new Response('Un fichier possèdant le même nom "' . $filename . ' exist déja. Veuillez choisir un autre nom');
        }

        return new Response('Le fichier '. $filename . ' a été ajouté');
    }

    /**
     * @Route("/write/{filename}/{text}")
     */
    public function writeAction($filename, $text)
    {

        if ($this->filesystem->exists($this->files_path.$filename))
        {
            $this->filesystem->appendToFile($this->files_path. $filename, $text);
        }
        else
        {
            return new Response('Aucun fichier possèdant le nom' . $filename . ' n\'exist. Veuillez vérifier le nom');
        }

        return new Response('Le contenu a été ajouté dans le fichier '. $filename);
    }

    /**
     * @Route("/copy/{oldname}/{newname}")
     */
    public function copyAction($oldname, $newname)
    {
        
        if ($this->filesystem->exists($this->files_path.$oldname))
        {
            $this->filesystem->copy($this->files_path.$oldname, $this->files_path.$newname, true);
        }
        else
        {
            return new Response('Aucun fichier possèdant le nom' . $oldname . ' n\'exist. Veuillez vérifier le nom');
        }

        return new Response('Le fichier '. $oldname . ' a été copié');

    }

    /**
     * @Route("/delete/{filename}")
     */
    public function deleteAction($filename)
    {
        if ($this->filesystem->exists($this->files_path.$filename))
        {
            $this->filesystem->remove($this->files_path.$filename);
        }
        else
        {
            return new Response('Aucun fichier possèdant le nom' . $filename . ' n\'exist. Veuillez vérifier le nom');
        }

        return new Response('Le fichier '. $filename . ' a été supprimé');
    }

    /**
     * @Route("/create-file/{filename}")
     */
    public function creationAction($filename, EventDispatcherInterface $eventDispatcher)
    {
        $files = $this->FileSystemImproved->createFile($filename);
        $event = new FileCreationEvent();
        $eventDispatcher->dispatch($event::MESSAGE, $event);
        return new JsonResponse(json_encode($files));

    }

    /**
     * @Route("/delete-file/{filename}")
     */
    public function removingAction($filename)
    {
        $ok = $this->FileSystemImproved->deleteFile($filename);
        return new JsonResponse(json_encode($ok));
    }

    /**
     * @Route("/write-in-file/{filename}/{text}")
     */
    public function writingAction($filename, $text)
    {
        $ok = $this->FileSystemImproved->writeInFile($filename, $text);
        return new JsonResponse(json_encode($ok));
    }

    /**
     * @Route("/read-file/{filename}")
     */
    public function readingAction($filename)
    {
        $ok = $this->FileSystemImproved->readInFile($filename);
        return new JsonResponse(json_encode($ok));
    }
}
