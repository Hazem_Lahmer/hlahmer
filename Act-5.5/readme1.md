﻿si nodeJS et yarn ne sont pas installés il faut les télécharger puis les installer : 

- https://nodejs.org/en/download/

- https://yarnpkg.com/lang/en/docs/install/#windows-stable

Après executer les deux commandes suivantes :

- Installation nodeJS

- Installatio package Yarn

-------------------------------------------------------------------------------------------------------------

commande à executer avant de commencer : 

- composer install (pour installer les fichiers manquants)

- bin/console doctrine:database:create (pour créer la BDD)

- bin/console make:migration

- bin/console doctrine:migrations:migrate

- yarn add @symfony/webpack-encore --dev

- composer require symfony/webpack-encore-bundle

- yarn add sass-loader@^7.0.1 node-sass --dev

- yarn add webpack-notifier@^1.6.0 --dev

- ./node_modules/.bin/encore dev

- yarn encore dev --watch

commande pour démarrer le serveur : php -S 127.0.0.1:8000 -t public

-----------------------------------------------------------------------------------------------------------------

Act-5.5.1

lien pour tester notre controlleur "src/Controller/PageController.php" : http://127.0.0.1:8000/

-----------------------------------------------------------------------------------------------------------------------------

Act-5.5.2

fichiers concernés : PostsController.php / Posts.php / PostType.php / index.html.twig 

lien pour tester notre controlleur "src/Controller/PostsController.php" : http://127.0.0.1:8000/posts

-----------------------------------------------------------------------------------------------------------------------------

Act-5.5.3

fichiers concernés : ImageController.php , assets/css/image.css , assets/js/image.js , templates/image/image.html.twig

route à executer : /image (mettre le curseur sur l'image puis cliquer)

-----------------------------------------------------------------------------------------------------------------------------

Act-5.5.4

routes à éxecuter : http://127.0.0.1:8000/register  ,  http://127.0.0.1:8000/login  ,  http://127.0.0.1:8000/acceuil
