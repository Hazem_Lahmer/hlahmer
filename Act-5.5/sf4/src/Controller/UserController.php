<?php

namespace App\Controller;

use App\Form\PostType;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormRegistryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use App\Entity\User;
use App\Form\UserType;
use PhpParser\Node\Stmt\Echo_;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;



class UserController extends AbstractController
{
    // En charge de la connexion
    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {

        //Récupères les erreurs de connexion s'il y en a
        $error = $authenticationUtils->getLastAuthenticationError();

        // Récupères l'identifiant rentré par l'utilisateur
        $lastUsername = $authenticationUtils->getLastUsername();

        //Renvoie l'utilisateur sur la page d'acceuil si la connexion est échouée.
        return $this->render('user/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $entityManager)
    {
        // 1) Générons le formulaire à partir de notre UserType
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        // 2) Traitement du formulaire une fois envoyé
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3)Encodage du mot de passe
            $password = $passwordEncoder->encodePassword($user, $user->getPassword());

            $user->setPassword($password);

            // 4) sauvegarde de l'utilisateur en base de données!
            $entityManager->persist($user);
            $entityManager->flush();    
         
            return $this->redirectToRoute('login');
        }
        //En cas d'erreur on reste sur le formulaire
        return $this->render(
            'user/register.html.twig',
            array('form' => $form->createView())
        );
    }

    //Now notre dernier contrôleur celui de la page d'accueil interne

    /**
     * @Route("/acceuil", name="acceuil")
     */
    public function acceuil()
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        return $this->render('user/acceuil.html.twig');
    }

    /**
     * @Route("/deconnexion", name="deconnexion")
     */
    public function logout()
    {
    }
}
