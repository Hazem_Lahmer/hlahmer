<?php

namespace App\Controller;

use App\Form\PostType;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormRegistryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use App\Entity\Post;

class PostsController extends AbstractController
{
    /**
     * @Route("/posts", name="posts")
     */
    public function index(EntityManagerInterface $entityManager, PostRepository $postRepository, Request $request, Environment $twig,FormFactoryInterface $formFactory, FormRegistryInterface $doctrine)
    {
        $posts = $postRepository->findAll();
        $post = new Post();

        $form = $this->createForm(PostType::class, $post);

        // Si la requête est en POST
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid() && $form->isSubmitted()) {
                $post = $form->getData();
                $entityManager->persist($post);
                $entityManager->flush();

                $this->addFlash('success', 'success');

                // On redirige vers la page de visualisation de l'annonce nouvellement créée
                return $this->redirectToRoute('posts');
            }
        }

        return new Response($twig->render ('posts/index.html.twig', [
            'posts' => $posts,
            'form' => $form->createView()
        ]));
    }
}
