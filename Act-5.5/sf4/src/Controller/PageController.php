<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;


class PageController 
{
    public function index()
    {
        return new Response('HELLO WORLD');
    }

    /**
     * @Route("/welcome")
     * @param Environment $twig
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function indexWelcome(Environment $twig)
    {
        return new Response($twig->render('pages/welcome.html.twig'));
    }


}