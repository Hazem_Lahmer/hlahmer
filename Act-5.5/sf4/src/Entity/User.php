<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="cet adresse est déjà utilisée")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *
     * @Assert\NotBlank(message = "Veillez saisir votre pseudo")
     *  
     * @ORM\Column(name="username", type="string")
     */
    private $username;

    /**
     * @ORM\Column(type="string")
     * 
     * @Assert\NotBlank(message = "Veillez saisir votre adresse mail")
     * 
     * @Assert\Regex(pattern="/^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/",
     * message="Votre adresse mail devra être sous la forme x@xx.xx")
     * 
     */
    private $email;

    /**
     *
     * @Assert\EqualTo(propertyPath="password", message = "les mots de passe doivent être identiques")
     * 
     */
    private $confirmedpassword;

    /**
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="array")
     */
    private $roles;

    public function __construct()
    {
        return array(0);
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getConfirmedPassword()
    {
        return $this->confirmedpassword;
    }

    public function setConfirmedPassword($confirmedpassword)
    {
        $this->confirmedpassword = $confirmedpassword;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getSalt()
    {
        // Les algorithmes bcrypt et argon2i ne nécessites pas de sel .
        // Vous pourriez avoir besoin de cette fonction si vous changez d'algo.
        return null;
    }

    /**
     * @see UserInterface
     */
    public function getRoles()
    {
        $roles = $this->roles;
        if ($roles != NULL) {
            return explode(" ", $roles);
        } else {
            return $this->roles;
        }
    }

    public function setRoles($roles)
    {
        $this->roles = $roles;
       
    }


    public function eraseCredentials()
    {
        return null;
    }
}
