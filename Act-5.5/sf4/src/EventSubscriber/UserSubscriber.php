<?php
namespace App\EventSubscriber;

use App\Entity\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;

class UserSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return [ Events::postPersist ];
    }
    
    public function postPersist()
    {
        dump ("SUBSCRIBER : VOUS ÊTES INSCRIT");
    }
}