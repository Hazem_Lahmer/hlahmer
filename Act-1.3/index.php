<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>PAGE D'ACCEUIL</title>
    </head>
 
    <body>

     <!-- Le fichier qui contient les articles -->
    
     <?php include("utils.php"); ?>
    
    <!-- L'en-tête -->
    
        <?php include("header.php"); ?>
    
    <!-- Le corps -->
    
    <section>
        
        <p><strong> seulement les 3 articles avant la date d'aujourdhui</strong></p>
        <p><strong> pour visualiser le reste des articles avant cette date, dirigez-vous vers la pages ARTICLES</strong></p>
        <p> <?php getArticles(3); ?> </p>
    
    </section>
    
    <!-- Le pied de page -->
    
    <?php include("footer.php"); ?>
    
    </body>
</html>