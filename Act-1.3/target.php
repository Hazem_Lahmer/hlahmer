<?php
    if(isset ($_POST['title']) && isset($_POST['text']) && isset($_POST['author']) && isset($_POST['publication_date']))
    {

        $added_article = array(
            'id' => uniqid(),
            'title' => $_POST['title'],
            'text' => $_POST['text'],
            'author' => $_POST['author'],
            'publication_date' => $_POST['publication_date']
        );

        $added_article = json_encode($added_article). "\n";

        $myfile = fopen('articles.json', 'a+'); // On ouvre le fichier
                
        fputs($myfile, $added_article); // On écrit le nouvel article 
        
        fclose($myfile); // On ferme le fichier 

 
        header('Location: articles.php');

    }
?>