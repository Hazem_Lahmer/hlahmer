<?php
    if(isset ($_GET['id']))
    {
        $myfile = fopen('articles.json', 'r+');
            
            $i=0;
            $j=0;

            while ($i < count(file('articles.json')))
            {
                $ligne = fgets($myfile);
                $article[$i] = json_decode($ligne, true) ;
                $i++;    
            }

        fclose($myfile);

        $otherfile = fopen('otherfile.json', 'a+'); // On ouvre le fichier
            
            $a = count($article);            
            for($i=0;$i<$a;$i++)
            {
                if ($article[$i]['id'] != $_GET['id'])
                {
                    $temp[$j] = $article[$i];
                    $added_temp = json_encode($temp[$j]). "\n";
                    fputs($otherfile, $added_temp); // On écrit le nouvel article 
                    $j++;
                }
            }

        fclose($otherfile); // On ferme le fichier 

        $otherfile = fopen('otherfile.json', 'r+'); // On ouvre le fichier

            $content = file_get_contents('otherfile.json', true);

        fclose($otherfile); // On ferme le fichier 


        // PHP program to delete a file named article.json  
        
        $file_pointer = "articles.json";  
        
        if (!unlink($file_pointer)) 
        {  
            echo ("$file_pointer cannot be deleted due to an error");  
        }  
        else 
        {  
            echo ("$file_pointer has been deleted");  
        }  
        

        // PHP program to put the content of otherfile.json in article.json

        $replacingfile = fopen('articles.json', 'a+');

            file_put_contents('articles.json', $content);

        fclose($replacingfile); 


        // PHP program to delete a file named otherfile.json 

        $second_file_pointer = "otherfile.json";  
        
        if (!unlink($second_file_pointer)) 
        {  
            echo ("$second_file_pointer cannot be deleted due to an error");
            
        }  
        else 
        {  
            header('Location: articles.php');  
        }                                        

    }