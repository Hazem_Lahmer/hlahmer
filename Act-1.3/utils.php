<?php

    function getArticles($a=null)
    {
        //on met le contenu du fichier dans notre tableau des articles
        $myfile = fopen('articles.json', 'r+');

        $i=0;
        while ($i < count(file('articles.json')))
            {
                
                $ligne = fgets($myfile);
                $article[$i] = json_decode($ligne, true) ;
                $i++;    
            }
        fclose($myfile);

        //vérifier $a par rapport à la taille du tableau
        $n = count($article);
        if (($a>$n)||($a==null))
        {
            $a=$n;
        }

        // on fait le tri du tableau selon la colonne des dates
        // Obtient une liste de la colonne des dates
        foreach ($article as $key => $row) 
        {
            $pubdate[$key]  = $row['publication_date'];
        }
        // Ajoute $article en tant que dernier paramètre, pour trier par la clé commune
        array_multisort($pubdate, SORT_DESC, $article);

        //on affiche les éléments du tableau dont la date n'est pas supérieur à la date actuel
        for($i=0;$i<$a;$i++)
        {
            if ($i<$n) // par ce qu'on a augmenté $a et on peut dépasser la taille du tableau
            {
                if (date("Y-m-d")>$article[$i]['publication_date'])
                {
                    foreach ($article[$i] as $cle=>$value)
                    {
                        echo '<p><strong>'.$cle.'</strong>'.' : '.$value.'</p>';
                    }
                    ?>
                    <p>(<a href="delete.php?id=<?php echo $article[$i]['id']; ?>">Supprimer</a>)</p>
                    <?php
                    echo '<br />';
                }
                else // si la date est dans le futur on augmente le nombre d'article à lire pour avoir le nombre d'article demandé par l'utilisateur
                {
                    $a++;
                }
            }
        }
    }

?>