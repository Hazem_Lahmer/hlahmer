<?php

    $article1 = array(
        'title' => 'titre1',
        'text' => 'Texte1',
        'author' => 'Aut1',
        'publication_date' => date("Y-m-d", mktime(0, 0, 0, 7, 1, 2000))
    );

    $article2 = array(
        'title' => 'titre2',
        'text' => 'Texte2',
        'author' => 'Aut2',
        'publication_date' => date("Y-m-d", mktime(0, 0, 0, 5, 25, 2020))
    );
        
    $article3 = array(
        'title' => 'titre3',
        'text' => 'Texte3',
        'author' => 'Aut3',
        'publication_date' => date("Y-m-d", mktime(0, 0, 0, 12, 22, 2013))
    );

    $article4 = array(
        'title' => 'titre4',
        'text' => 'Texte4',
        'author' => 'Aut4',
        'publication_date' => date("Y-m-d", mktime(0, 0, 0, 6, 9, 2010))
    );

    $article5 = array(
        'title' => 'titre5',
        'text' => 'Texte5',
        'author' => 'Aut5',
        'publication_date' => date("Y-m-d", mktime(0, 0, 0, 19, 10, 1993))
    );

    $article6 = array(
        'title' => 'titre6',
        'text' => 'Texte6',
        'author' => 'Aut6',
        'publication_date' => date("Y-m-d", mktime(0, 0, 0, 19, 10, 2001))
    );

    $article7 = array(
        'title' => 'titre7',
        'text' => 'Texte7',
        'author' => 'Aut7',
        'publication_date' => date("Y-m-d", mktime(0, 0, 0, 19, 10, 2021))
    );

    $article = array($article1, $article2, $article3, $article4, $article5, $article6, $article7);
    
    $n = count($article);

    function getArticles($a=null)
    {
        global $article, $article1, $article2, $article3, $article4, $article5, $n;

        if (($a>$n)||($a==null))
        {
            $a=$n;
        }
        // Obtient une liste de la colonne des dates
        foreach ($article as $key => $row) 
        {
            $pubdate[$key]  = $row['publication_date'];
        }

        // Trie les données par rang décroissant
        // Ajoute $article en tant que dernier paramètre, pour trier par la clé commune
        array_multisort($pubdate, SORT_DESC, $article);

        for($i=0;$i<$a;$i++)
        {
            if ($i<$n)
            {
                if (date("Y-m-d")>$article[$i]['publication_date'])
                {
                    foreach ($article[$i] as $cle2=>$value2)
                    {
                        echo '<p><strong>'.$cle2.'</strong>'.' : '.$value2.'</p>';
                    }
                    echo '<br />';
                }
                else
                {
                    $a++;
                }
            }
        }
    }
    
?>
