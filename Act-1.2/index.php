<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link href="style.css" rel="stylesheet" />
        <title>PAGE D'ACCEUIL</title>
    </head>
 
    <body>

     <!-- Le fichier qui contient les articles -->
    
     <?php include("utils.php"); ?>
    
    <!-- L'en-tête -->
    
        <?php include("header.php"); ?>
    
    <!-- Le corps -->
    
    <section>
        
        <p> <?php getArticles(3); ?> </p>
    
    </section>
    
    <!-- Le pied de page -->
    
    <?php include("footer.php"); ?>
    
    </body>
</html>