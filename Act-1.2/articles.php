<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link href="style.css" rel="stylesheet" />
        <title>ARTICLES</title>
    </head>
 
    <body>
        
        <!-- Le fichier qui contient les articles -->
        
        <?php include("utils.php"); ?>

        <!-- L'en-tête -->
    
        <?php include("header.php"); ?>
    
        <!-- Le corps -->
    
        <?php getArticles(); ?>

     
    </body>
</html>