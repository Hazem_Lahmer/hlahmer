<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Category;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Repository\ProduitRepository;
use AppBundle\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;

class CategoryController extends Controller
{
    
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    /**
     * @Route("/createcategorie/{categoryname}", name="categoriecreationpage")
     */
    public function createCategorieAction($categoryname)
    {

        $category = new Category($categoryname);
        $category->setName($categoryname);

        $this->entityManager->persist($category);
        $this->entityManager->flush();

        return new Response(
            ' and new category with id: ' . $category->getId()
        );
    }
}
