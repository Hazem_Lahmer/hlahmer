<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Category;
use AppBundle\Entity\Produit;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Repository\ProduitRepository;
use AppBundle\Repository\CategoryRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class ProduitController extends Controller
{
    
    private $produitRepository;
    private $categoryRepository;
    private $entityManager;

    public function __construct(ProduitRepository $produitRepository, CategoryRepository $categoryRepository, EntityManagerInterface $entityManager)
    {
        $this->produitRepository = $produitRepository;
        $this->categoryRepository = $categoryRepository;
        $this->entityManager = $entityManager;
    }
     
    /**
     * @Route("/creerproduit/{productname}/{categoryname}", name="produitcreationpage")
     */
    public function createproduitAction($productname,$categoryname)
    {
        $category = $this->categoryRepository->findByName($categoryname);
        if (!$category)
        {
            $category = new Category($categoryname);
        }

        $produit = $this->produitRepository->findByNameAndCategory($productname, $category->getId());
        if (!$produit)
        {
            $produit = new Produit($productname, $category);

            $this->entityManager->persist($category);
            $this->entityManager->persist($produit);
            $this->entityManager->flush();

            return new Response(
                'Saved new produit with id: ' . $produit->getId()
                    . ' and new category with id: ' . $category->getId()
            );
        }
        else
        {
            return new Response('This Product Cannot be Saved. This product name "'
            .$productname.'" is already existing in same category. Try with another product name or another category !!!'); 
        }
    }

    /**
     * @Route("/getproduits/{categoryId}", name="categorieproduits")
     */
    public function showproduitsAction($categoryId)
    {
        $category = $this->categoryRepository->findCatgegoryById($categoryId);

        $produits = $category->getproduits();
        return $this->render('produits.html.twig', [
            'produits' => $produits,
        ]);
    }

    /**
     * @Route("/detail/{produitId}", name="produitsdetails")
     */
    public function showAction($produitId)
    {
        $produit = $this->produitRepository->findOneByIdJoinedToCategory($produitId);
        $category = $produit->getCategory();
        $produits = $category->getProduits();
        return $this->render('produitdetails.html.twig', [
            'produits' => $produits,
            'id' => $produit->getId(),
            'nom' => $produit->getNom(),
        ]);
    }
}
