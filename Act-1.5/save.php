<?php

    try
    {
        // On se connecte à MySQL en activant les erreurs
        $bdd = new PDO('mysql:host=localhost;dbname=database;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));    
    }
    catch(Exception $e)
    {
        // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
    }

    // Si tout va bien, on peut continuer

    if ($_POST['pass'] === $_POST['pass_confirme'])
    {
        if (preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $_POST['email'])) // on vérifie la forme de l'adresse mail
        {
            // On enregistre les donnees dans la base et On rend inoffensives les balises HTML que le visiteur a pu rentrer
            $pseudo = htmlspecialchars($_POST['pseudo']);
            $pass = htmlspecialchars($_POST['pass']);
            $pass_confirme = htmlspecialchars($_POST['pass_confirme']);
            $email = htmlspecialchars($_POST['email']);
            
            // on hache le mot de passe
            $pass_hache = password_hash($pass, PASSWORD_DEFAULT);


            // On insere dans la table membres
            $req = $bdd->prepare('INSERT INTO membres(pseudo, pass, email, inscription_date) VALUES (:pseudo, :pass, :email, CURDATE())');
            $req->execute(array('pseudo' => $pseudo, 'pass' => $pass_hache, 'email' => $email));

            session_start();
            $_SESSION['pseudo'] = $pseudo;
            $_SESSION['pass'] = $pass_hache;
            $_SESSION['email'] = $email;

            // Puis rediriger vers notre page d'acceuil
            header('Location: index.php');
        }
        else
        {
            header('Location: inscription.php');
        }

    }
    else
    {?>
        <meta http-equiv="refresh" content="0; URL=inscription.php" />
    <?php
    }

?>