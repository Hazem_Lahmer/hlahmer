<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>connexion</title>
    </head>
    <style>

        body 
        {
            padding-top: 50px;
        }
        
        .form_col 
        {
            display: inline-block;
            margin-right: 15px;
            margin-left: 450px;
            padding: 8px 0px;
            width: 200px;
            min-height: 1px;
            text-align: right;
        }

    </style>
    <body>

        <form action="verif_connexion.php" method="post">
            <p>
                <label class="form_col" for="pseudo">Pseudo</label> : <input type="text" name="pseudo" id="pseudo"/> <br />
                
                <label class="form_col" for="pass">Mot de passe</label> :  <input type="password" name="pass" id="pass"/> <br />
                
                <label class="form_col" for="case">Retenir mon identifiant</label> <input type="checkbox" name="case" id="case" /> <br />
                
                <span class="form_col"></span>
                <input type="submit" value="Se connecter" /><br />

                <span class="form_col"></span>
                <a href="inscription.php">S'inscrire</a>
            </p>
        </form>

        <?php

            if ((isset($_COOKIE['pseudo'])) && (isset($_COOKIE['pass'])))
            {
                $pseudo = $_COOKIE['pseudo'];
                $pass = $_COOKIE['pass'];

                echo "<script>
                document.getElementById('pseudo').value = '$pseudo';
                document.getElementById('pass').value = '$pass';
                </script>";
            }
        
        ?>
    </body>
</html>