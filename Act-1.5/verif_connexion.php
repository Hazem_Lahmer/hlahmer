<?php 

    try
    {
        // On se connecte à MySQL en activant les erreurs
        $bdd = new PDO('mysql:host=localhost;dbname=database;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));    
    }
        catch(Exception $e)
    {
        // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
    }

    //on vérifie les informations envoyée par le visiteur

    if (isset($_POST['pseudo']) AND ($_POST['pseudo']!='') AND isset($_POST['pass']) AND ($_POST['pass']!=''))
    {
        // On enregistre les donnees dans la base et On rend inoffensives les balises HTML que le visiteur a pu rentrer
                            
        $pseudo = htmlspecialchars($_POST['pseudo']);
        $pass = htmlspecialchars($_POST['pass']);


        //  Récupération de l'utilisateur et de son pass hashé
        $req = $bdd->prepare('SELECT * FROM membres WHERE pseudo = :pseudo');
        $req->execute(array('pseudo' => $pseudo));
        $resultat = $req->fetch();
        
        $_POST['pass'] = password_hash($_POST['pass'], PASSWORD_DEFAULT);

        if (isset($_POST['case']))
        {
            setcookie('pseudo', $pseudo, time() + 30*24*3600, null, null, false, true);
            setcookie('pass', $pass, time() + 30*24*3600, null, null, false, true);

            // Comparaison du pass envoyé via le formulaire avec la base
            $isPasswordCorrect = password_verify($pass , $resultat['pass']);

            if (!$resultat)
            {
                echo 'Mauvais identifiant !';
            }
            else
            {
                if ($isPasswordCorrect) 
                {
                    session_start();
                    $_SESSION['pseudo'] = $pseudo;
                    $_SESSION['pass'] = $resultat['pass'];
                    $_SESSION['email'] = $resultat['email'];

                    header('Location: index.php');
                }
                else 
                {
                    echo 'Mauvais mot de passe !';
                }
            }
        }
        else
        {
            // Suppression des cookies de connexion automatique
            setcookie('pseudo', '');
            setcookie('pass', '');
            
            // Comparaison du pass envoyé via le formulaire avec la base
            $isPasswordCorrect = password_verify($pass , $resultat['pass']);

            if (!$resultat)
            {
                echo 'Mauvais identifiant !';
            }
            else
            {
                if ($isPasswordCorrect) 
                {
                    session_start();
                    $_SESSION['pseudo'] = $pseudo;
                    $_SESSION['pass'] = $resultat['pass'];
                    $_SESSION['email'] = $resultat['email'];

                    header('Location: index.php');
                }
                else 
                {
                    echo 'Mauvais mot de passe !';
                }
            } 
        }
    }
    else
    {
        header('Location: connexion.php');
    }
?>