<?php
    
    session_start();
?>

<!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8" />
            <title>inscription</title>
        </head>
        <style>
        body 
        {
            padding-top: 50px;
            padding-left: 50px;
        }
        </style>
        <body>
            
            <?php
            if ((isset($_SESSION['pseudo'])) && ($_SESSION['email']) && ($_SESSION['pass']))
            {?>
                <p>Bienvenue <strong><?php echo $_SESSION['pseudo']?></strong>.</p>
                    <p>Tu viens de créer un nouveau compte.</p>
                    <p>Merci pour la confiance.</p>
                    <p>Tu ne peux que déconnecter pour le moment !!</p>
                    <p><a href="deconnexion.php">Deconnexion</a><br /></p>
                    <?php
            }
            else
            {
                header('Location: connexion.php');
            }?>

        </body>
    </html>