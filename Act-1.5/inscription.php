<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="style.css">
        <title>inscription</title>
    </head>

    <body>

        <form id="myForm" action="save.php" method="post">

            <label class="form_col" for="pseudo">Pseudo :</label>
            <input name="pseudo" id="pseudo" type="text" /><br />
            <br /><br />

            <label class="form_col" for="pass">Mot de passe :</label>
            <input name="pass" id="pass" type="password" /><br />
            <br /><br />

            <label class="form_col" for="pass_confirme">Mot de passe (confirmation) :</label>
            <input name="pass_confirme" id="pass_confirme" type="password" /><br />
            <br /><br />

            <label class="form_col" for="email">Adresse Mail :</label>
            <input name="email" id="email" type="text" /><br />
            <br /><br />

            <span class="form_col"></span>
            <input type="submit" value="S'inscrire" /> <input type="reset" value="Réinitialiser le formulaire" />

        </form>
        
    </body>
</html>