<?php
    
    require_once("Carte.php");
    require_once("DamageableInterface.php");
    class Monstre extends Carte implements DamageableInterface
    {
        private $_vie ;
        
        // initialisation des données
        public function __construct()
        {
          $this->setDegats();
          $this->setVie();
          $this->setMana();
        }
        
        // Définition de la méthode implémenté de l'interface
        public function takeDamages(int $n)
        {
            if ($this->_vie >= $n)
            {
                $this->_vie -= $n ;
            }
            else
            {
                $this->_vie = 0;
            }
        }

        //Une fonction « attaquer » qui prendra un paramètre de type damageableInterface et qui lui retirera des points de vie correspondants au nombre de points de dégâts de l’attaquant
        public function attaquer(DamageableInterface $DamageableInterface)
        {
            $DamageableInterface->takeDamages($this->_degats);       
        }

        // GETTERS      
        public function vie()
        {
            return $this->_vie;
        }      

        // SETTERS       
        public function setVie()
        {           
            $this->_vie = random_int(0, 9)+1;
        }        
    }

?>