<?php
    
    class Carte
    {
        protected $_mana, $_degats;
        
        // GETTERS
        public function degats()
        {
            return $this->_degats;
        }
        
        public function mana()
        {
            return $this->_mana;
        }
        

        // SETTERS
        public function setDegats()
        {            
            $this->_degats = random_int(0, 10);
        }
        
        public function setMana()
        {         
            $this->_mana = random_int(0, 10);
        }
    }

?>