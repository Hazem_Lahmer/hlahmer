<?php
    
    function chargerClasse($classe)
    {
        require_once $classe . '.php'; // On inclut la classe correspondante au paramètre passé.
    }

    spl_autoload_register('chargerClasse');

    $p = new Plateau ('SAHAR', 'HAZEM');

    echo $p . '<br />';

    $monstre1 = new Monstre() ;
    $monstre2 = new Monstre() ;
    $monstre3 = new Monstre() ;
    $monstre4 = new Monstre() ;
    $monstre5 = new Monstre() ;

    $monstre6 = new Monstre() ;
    $monstre7 = new Monstre() ;
    $monstre8 = new Monstre() ;
    $monstre9 = new Monstre() ;
    $monstre10 = new Monstre() ;

    $p->joueurA()->placerMonstre($monstre1);
    $p->joueurA()->placerMonstre($monstre2);
    $p->joueurA()->placerMonstre($monstre3);
    $p->joueurA()->placerMonstre($monstre4);
    $p->joueurA()->placerMonstre($monstre5);

    $p->joueurB()->placerMonstre($monstre6);
    $p->joueurB()->placerMonstre($monstre7);
    $p->joueurB()->placerMonstre($monstre8);
    $p->joueurB()->placerMonstre($monstre9);
    $p->joueurB()->placerMonstre($monstre10);

    for($i=1; $i<=$p->joueurA()->nombreMonstres(); $i++)
    {
        echo ('<p>Monstre num ' . $i . ' du joueur A<p>') ;        
    }
    
    echo '<br />';

    for($i=1; $i<=$p->joueurB()->nombreMonstres(); $i++)
    {
        echo ('<p>Monstre num ' . $i . ' du joueur B</p>') ;        
    }

?>