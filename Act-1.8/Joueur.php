<?php

    require_once("Monstre.php");
    require_once("Sort.php");
    require_once("DamageableInterface.php");

    class Joueur implements DamageableInterface
    {
        private $_main, $_nbMonstre, $_mana, $_vie, $_pseudo, $_monstresPlace ;
        
        // initialisation des données
        public function __construct($pseudo, $nbMonstreParJoueur=5)
        {
            $this->_monstresPlace = array();
            $this->_main = array();
            $this->setPseudo($pseudo);
            $this->setNbMonstre($nbMonstreParJoueur);
            $this->_vie = 30;
            $this->_mana = 10;
        }
        
        // Définition de la méthode implémenté de l'interface
        public function takeDamages(int $n)
        {
            if ($this->_vie >= $n)
            {
                $this->_vie -= $n ;
            }
            else
            {
                $this->_vie = 0;
            }
        }

        //methode qui joue les cartes
        public function jouer (Joueur $joueur, int $x)
        {
            if ($this->_main[$x] instanceof Sort)
            {
                $this->_main[$x]->attaquer($joueur);
            }
            $this->_mana--;
            unset($this->_main[$x]);
        }
        
        // Pour récupérer le nombre des monstres disponibles
        public function nombreMonstres()
        {
            return count($this->_monstresPlace);
        }

        // une fonction pour ajouter un monstre dans monstrePlace
        public function placerMonstre (Monstre $monstre)
        {
            if (count($this->_monstresPlace) < $this->_nbMonstre)
            {
                array_push($this->_monstresPlace, $monstre);
            }
        }

        // une fonction pour ajouter un sort dans la main
        public function piocher (Sort $sort)
        {
            array_push($this->_main, $sort);
        }      

        // GETTERS
        public function montrerMain()
        {
            for($i=1; $i <= count($this->_main); $i++)
            {
                echo 'la carte numéro ' . $i . '<br />' ;
            }
        }

        public function pseudo()
        {
            return $this->_pseudo;
        }
        
        public function vie()
        {
            return $this->_vie;
        }
        
        public function mana()
        {
            return $this->_mana;
        }

        // SETTERS
        public function setPseudo($pseudo)
        {
          if (is_string($pseudo))
          {
            $this->_pseudo = $pseudo;
          }
        }

        public function setNbMonstre($nbMonstreParJoueur)
        {
            $nbMonstreParJoueur = (int) $nbMonstreParJoueur;
            
            if ($nbMonstreParJoueur > 0)
            {
                $this->_nbMonstre = $nbMonstreParJoueur;
            }
        }
    }

?>