<?php
    
    require_once("Joueur.php");
    
    class Plateau
    {
        private static $_nbMonstreParJoueur;
        private $_joueurA, $_joueurB ;
        
        // initialisation des données
        public function __construct($joueurA, $joueurB, $nbMonstreParJoueur=5)
        {
            static::setNbMonstreParJoueur($nbMonstreParJoueur);
            $this->setJoueurs($joueurA, $joueurB);
        }

        // La fonction pour afficher le plateau dans main
        public function __toString()
        {
            return "<p>Nombre de monstres par personne : " . static::$_nbMonstreParJoueur . "</p><p>nom du joueur A: " . $this->_joueurA->pseudo() . "</p><p>nom du joueur B: " . $this->_joueurB->pseudo() . '</p>' ;
        }

        // GETTERS
        public function nbMonstreParJoueur()
        {
            return $this->_nbMonstreParJoueur;
        }
        
        public function joueurA()
        {
            return $this->_joueurA;
        }
        
        public function joueurB()
        {
            return $this->_joueurB;
        }

        // SETTERS
        public function setNbMonstreParJoueur($nbMonstreParJoueur)
        {
            $nbMonstreParJoueur = (int) $nbMonstreParJoueur;
            
            if ($nbMonstreParJoueur > 0)
            {
                static::$_nbMonstreParJoueur = $nbMonstreParJoueur;
            }
        }
        
        public function setJoueurs($joueurA, $joueurB)
        {            
            if ((is_string($joueurA)) && (is_string($joueurB)))
            {
                $this->_joueurA = new Joueur ($joueurA, static::$_nbMonstreParJoueur);
                $this->_joueurB = new Joueur ($joueurB, static::$_nbMonstreParJoueur);
            }
        }

        //ajout de l'énoncé
        function initialiser()
        {
            echo "Pseudo joueur A: " . $this->_joueurA->pseudo();

            echo "Pseudo joueur B: " . $this->_joueurA->pseudo();

            $Sorts = array();
            
            $Sort1 = new Sort() ;
            $Sort2 = new Sort() ;
            $Sort3 = new Sort() ;
            $Sort4 = new Sort() ;
            $Sort5 = new Sort() ;
            $Sort6 = new Sort() ;
            $Sort7 = new Sort() ;
            $Sort8 = new Sort() ;
            $Sort9 = new Sort() ;
            $Sort10 = new Sort() ;
            $Sort11 = new Sort() ;
            $Sort12 = new Sort() ;

            $this->_joueurA->piocher($Sort1);
            $this->_joueurA->piocher($Sort2);
            $this->_joueurA->piocher($Sort3);

            $this->_joueurB->piocher($Sort4);
            $this->_joueurB->piocher($Sort5);
            $this->_joueurB->piocher($Sort6);

            /*for($i = 0; $i < 3; $i++)
            {
                $this->joueurA->piocher();
                $this->joueurB->piocher();
            }*/

        }

        function lancer()
        {
            // appelle la fonction d'initialisation plus haut
            $this->initialiser();

            // affiche le plateau grace à la méthode magique __toString
            echo $this. '<br />';

            // Execute ce qu'il y a dans la boucle tant que les 2 joueurs sont vivants
            while ($this->_joueurA->vie() > 0 && $this->_joueurB->vie() > 0)
            {
                echo $this->_joueurA->pseudo() . " es-tu pret ? <br />";

                $handle = fopen ("php://stdin","r");

                $entree = fgets($handle);

                $this->_joueurA->piocher(new Sort());

                $this->_joueurA->montrerMain();

                echo $this->_joueurA->pseudo() ." : quelle carte veux-tu jouer ? choisis le chiffre<br />";

                $handle = fopen ("php://stdin","r");

                $entree = fgets($handle);

                $this->_joueurA->jouer($this->_joueurB, (int)$entree);


                echo $this . '<br />';

                
                echo $this->_joueurB->pseudo() . " es-tu pret ? <br />";

                $handle = fopen ("php://stdin","r");

                $entree = fgets($handle);

                $this->_joueurB->piocher(new Sort());

                $this->_joueurB->montrerMain();

                echo $this->_joueurB->pseudo() ." : quelle carte veux-tu jouer ? choisis le chiffre<br />";

                $handle = fopen ("php://stdin","r");

                $entree = fgets($handle);

                $this->_joueurB->jouer($this->_joueurA, (int)$entree);

                echo $this;
            }

            // Si on sort de la boucle c'est qu'un joueur est mort, ceci affiche le résultat
            if ($this->_joueurA->vie() <= 0 && $this->_joueurB->vie() <= 0)
            {
                echo "Egalite !";
            }
            else if ($this->_joueurA->vie() <= 0)
            {
                echo $this->_joueurA->pseudo() . ' est un gros loser';
            }
            else if ($this->_joueurB->vie() <= 0)
            {
                echo $this->_joueurB->pseudo() . ' est un gros loser';
            }
        }
    }

?>