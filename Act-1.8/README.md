﻿Act1

On a deux classes :

Monstre : attributs : id, vie, mana, dégats
	  méthodes : attaquer (qui prend les points de vie d'un monstre selon les dégats de l'attaquant) + GETTERS + SETTERS

Monstre : attributs : mana, dégats
	  méthodes : attaquer (qui prend les points de vie d'un monstre selon les dégats de l'attaquant) + GETTERS + SETTERS

---------------------------------------------------------------------------------------------------------------------------

Act2

Ajout de la classe Joueur : attributs : pseudo, vie, mana, monstresPlace
	  		    méthodes : placerMonstre(qui ajoute les monstres dans le tableau monstresPlace) + GETTERS + SETTERS

-----------------------------------------------------------------------------------------------------------------------------

Act3

Ajout de la classe Plateau : attributs : nbMonstreParJoueur, joueurA, joueurB
	  		     méthodes : GETTERS + SETTERS (setJoueurs créer deux instances de la classe Joueur)

-----------------------------------------------------------------------------------------------------------------------------

Act4

Ajout de la classe abstraite Carte dont hériteront les deux classes Sort et Monstre

-----------------------------------------------------------------------------------------------------------------------------

Act5

Ajout de l'interface DamageableInterface

Implémentation de l'interface dans la classe Monstre et modification de sa méthode attaquer

------------------------------------------------------------------------------------------------------------------------------

Act6

Dans un nouveau fichier main, on va afficher:
- le tableau par l'intermediaire de la fonction __tostring() (le nombre de monstres par joueur + les noms des joueurs)
- les monstres de chaques joueurs