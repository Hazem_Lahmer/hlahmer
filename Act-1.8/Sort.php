<?php

    require_once("Carte.php");
    require_once("DamageableInterface.php");

    class Sort extends Carte
    {
        // initialisation des données
        public function __construct()
        {
          $this->setDegats();
          $this->setMana();
        }
        
        //Une fonction « attaquer » qui prendra en paramètre une instance de Monstre et qui lui retirera des points de vie correspondants au nombre de points de dégâts de l’attaquant
        public function attaquer(DamageableInterface $DamageableInterface)
        {
            if ($DamageableInterface->_vie >= $this->_degats)
            {
                $DamageableInterface->_vie -= $this->_degats;
            }
            else
            {
                $DamageableInterface->_vie = 0;
            }  
        }
    }

?>