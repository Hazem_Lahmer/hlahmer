<?php

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Produit;

// ...
class ProduitController extends Controller
{

    /**
     * @Route("/edit-product/{id}/{label}/{price}/{quantity}", name="updatingpage")
     */
    public function updateAction($id, $label, $price, $quantity)
    {
        // you can fetch the EntityManager via $this->getDoctrine()
        // or you can add an argument to your action: createAction(EntityManagerInterface $entityManager)
        $entityManager = $this->getDoctrine()->getManager();
        $produit = $entityManager->getRepository(Produit::class)->findById($id);

        if (!$produit) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        }

        $produit->setLabell($label);
        $produit->setPrice($price);
        $produit->setQuantity($quantity);
        $entityManager->flush();

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/get-product/{id}", name="findingpage")
     */
    public function ShowProductById($id)
    {
        $produit = $this->getDoctrine()
            ->getRepository(Produit::class)->findById($id);
            
        return $this->render('produit/produit.html.twig', array(
            'produit' => $produit
        ));
    }

    /**
     * @Route("/get-all-products", name="homepage")
     */
    public function getproductsAction()
    {
        $repository = $this->getDoctrine()->getRepository(Produit::class);

        $produits = $repository->findAll();

        return $this->render('produit/produits.html.twig', [
            'produits' => $produits,
        ]);
    }
}
