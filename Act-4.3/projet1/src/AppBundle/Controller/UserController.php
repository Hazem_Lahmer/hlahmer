<?php

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\User;
use DateTime;
use Symfony\Component\HttpFoundation\Response;

// ...
class UserController extends Controller
{
    /**
     * @Route("/create", name="creationpage")
     */
    public function createAction()
    {
        // you can fetch the EntityManager via $this->getDoctrine()
        // or you can add an argument to your action: createAction(EntityManagerInterface $entityManager)
        $entityManager = $this->getDoctrine()->getManager();

        $user = new User();

        $date = new DateTime("1995-01-01 00:00:00");
        $user->setFirstname('Sahar');
        $user->setLastname('BEN KHOUD');
        $user->setEmail('hazem.lahmer@talan.com');
        $user->setAdress('Sousse');
        $user->setBirthdate($date);

        // tells Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($user);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();

        return new Response('Saved new user with id ' . $user->getId());
    }

    /**
     * @Route("/getall", name="showpage")
     */
    public function getallAction()
    {
        $repository = $this->getDoctrine()->getRepository(User::class);

        $users = $repository->findAll();

        return $this->render('user/user.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/get-user/{id}", name="id")
     */
    public function getByIdAction(int $id)
    {
        $repository = $this->getDoctrine()->getRepository(User::class);

        $user = $repository->findById($id);
        return $this->render('user/userbyid.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/cityshow/{city}", name="cityshow")
     */
    public function getAllByCityAction(string $city)
    {
        
        $repository = $this->getDoctrine()->getRepository(User::class);

        $users = $repository->findAllByCity($city);

        return $this->render('user/userbycity.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/getbyadressandemail/{address}/{email}", name="")
     */
    public function getByAddressAndEmailAction(string $address, string $email)
    {
        $repository = $this->getDoctrine()->getRepository(User::class);

        $users = $repository->findByAddressAndEmail($address, $email);

        return $this->render('user/userbyaddressandname.html.twig', [
            'users' => $users,
        ]);
    }
}
