<?php
    if(isset ($_POST['author']) && isset($_POST['texte']) && isset($_GET['id']))
    {
        
        // Connexion à la base de données
        try
        {
            $bdd = new PDO('mysql:host=localhost;dbname=database;charset=utf8', 'root', '');
        }
        catch(Exception $e)
        {
            die('Erreur : '.$e->getMessage());
        }

        $id = $_GET['id'];
        $texte = $_POST['texte'];
        $author = $_POST['author'];


        // Insertion
        $req = $bdd->prepare('INSERT INTO commentaires (article_id, texte, author, comment_date) VALUES (:article_id, :texte, :author, CURDATE())');
        $req->execute(array(
            'article_id' => $id,
            'texte' => $texte,
            'author' => $author));

        // Redirection du visiteur vers la page des articles
        header('Location: commentaires.php?id='.$id.'');

    }
?>
