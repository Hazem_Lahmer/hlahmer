<?php

    if(isset ($_GET['id']))
    {
        
        // Connexion à la base de données
        try
        {
            $bdd = new PDO('mysql:host=localhost;dbname=database;charset=utf8', 'root', '');
        }
        catch(Exception $e)
        {
                die('Erreur : '.$e->getMessage());
        }
        
        $id = $_GET['id'];
        $req = $bdd->prepare('DELETE FROM articles WHERE id = :id');
        $req->execute(array('id' => $id));
    }

    header('Location: articles.php');

?>  