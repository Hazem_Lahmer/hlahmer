<?php
    if(isset ($_POST['title']) && isset($_POST['texte']) && isset($_POST['author']) && isset($_POST['publication_date']))
    {
        
        // Connexion à la base de données
        try
        {
            $bdd = new PDO('mysql:host=localhost;dbname=database;charset=utf8', 'root', '');
        }
        catch(Exception $e)
        {
            die('Erreur : '.$e->getMessage());
        }

        $title = $_POST['title'];
        $texte = $_POST['texte'];
        $author = $_POST['author'];
        $publication_date = $_POST['publication_date'];


        // Insertion
        $req = $bdd->prepare('INSERT INTO articles(title, texte, author, publication_date) VALUES(:title, :texte, :author, :publication_date)');
        $req->execute(array(
            'title' => $title,
            'texte' => $texte,
            'author' => $author,
            'publication_date' => $publication_date));

        // Redirection du visiteur vers la page des articles
        header('Location: articles.php');

    }
?>
