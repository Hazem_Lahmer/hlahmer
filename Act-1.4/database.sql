-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 06 déc. 2019 à 15:18
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `database`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `texte` text NOT NULL,
  `author` varchar(255) NOT NULL,
  `publication_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `articles`
--

INSERT INTO `articles` (`id`, `title`, `texte`, `author`, `publication_date`) VALUES
(1, 'ARTICLE 1', 'C\'est le contenu du premier article ', 'Hazem', '2019-12-04'),
(2, 'ARTICLE 2', 'C\'est un simple article aussi\r\nrien de spécial', 'Sahar', '2019-11-14'),
(4, 'ARTICLE 4', 'cet article a une date supérieur à notre temps actuel', 'Wahib', '2019-12-24'),
(5, 'ARTICLE 5', 'C\'est l\'article qui parle de talan_academy', 'Ibtissem', '2019-09-19'),
(6, 'ARTICLE 6', 'je ne trouve pas les mots', 'Islem', '2019-07-04'),
(7, 'ARTICLE 7', 'cet article aussi est à afficher à partir du premier janvier 2020', 'Nesrine', '2020-01-01');

-- --------------------------------------------------------

--
-- Structure de la table `commentaires`
--

DROP TABLE IF EXISTS `commentaires`;
CREATE TABLE IF NOT EXISTS `commentaires` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `texte` text NOT NULL,
  `author` varchar(255) NOT NULL,
  `comment_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `commentaires`
--

INSERT INTO `commentaires` (`id`, `article_id`, `texte`, `author`, `comment_date`) VALUES
(1, 1, 'La vie c\'est des étapes... La plus douce c\'est l\'amour... La plus dure c\'est la séparation... La plus pénible c\'est les adieux... La plus belle c\'est les retrouvailles.', 'sahar', '2019-09-11'),
(2, 1, 'Dans la vie on ne fait pas ce que l\'on veut mais on est responsable de ce que l\'on est.', 'wahib', '2019-08-14'),
(3, 1, 'La vie est un mystère qu\'il faut vivre, et non un problème à résoudre.', 'ibtissem', '2019-08-14'),
(4, 1, 'L\'une des plus grandes douleurs est d\'aimer une personne que tu ne peux pas avoir.\r\n\r\n', 'marouane', '2019-12-09'),
(5, 2, 'On passe une moitié de sa vie à attendre ceux qu\'on aimera et l\'autre moitié à quitter ceux qu\'on aime.\r\n\r\n', 'nesrine', '2019-09-24'),
(6, 2, 'La vie, c\'est comme une bicyclette, il faut avancer pour ne pas perdre l\'équilibre.', 'islem', '2019-01-21'),
(7, 2, 'Pour critiquer les gens il faut les connaître, et pour les connaître, il faut les aimer.\r\n\r\n', 'hazem', '2019-12-01'),
(8, 2, 'La règle d\'or de la conduite est la tolérance mutuelle, car nous ne penserons jamais tous de la même façon, nous ne verrons qu\'une partie de la vérité et sous des angles différents.\r\n\r\n', 'amine', '2018-11-14'),
(9, 5, 'Choisissez un travail que vous aimez et vous n\'aurez pas à travailler un seul jour de votre vie.\r\n\r\n', 'mouna', '2019-07-15'),
(10, 6, 'Le monde est dangereux à vivre ! Non pas tant à cause de ceux qui font le mal, mais à cause de ceux qui regardent et laissent faire.\r\n\r\n', 'mouna', '2018-09-12'),
(11, 4, 'Il est difficile de dire adieu lorsqu\'on veut rester, compliqué de rire lorsqu\'on veut pleurer, mais le plus terrible est de devoir oublier lorsqu\'on veut aimer.\r\n\r\n', 'ibtissem', '2019-12-30'),
(12, 3, 'On ne souffre jamais que du mal que nous font ceux qu\'on aime. Le mal qui vient d\'un ennemi ne compte pas.\r\n\r\n', 'hazem', '2019-11-20'),
(13, 5, 'Le sentiment de ne pas être aimé est la plus grande des pauvretés.\r\n\r\n', 'sahar', '2019-12-28'),
(14, 5, 'Dans la vengeance et en amour, la femme est plus barbare que l\'homme.\r\n\r\n', 'marouane', '2019-08-29'),
(15, 5, 'Un seul être vous manque et tout est dépeuplé.\r\n\r\n', 'wahib', '2018-12-13'),
(16, 3, 'Aimer, ce n\'est pas se regarder l\'un l\'autre, c\'est regarder ensemble dans la même direction.\r\n\r\n', 'wahib', '2019-04-07'),
(17, 4, 'La nature fait les hommes semblables, la vie les rend différents.\r\n\r\n', 'islem', '2019-05-06'),
(18, 6, 'L\'homme veut être le premier amour de la femme, alors que la femme veut être le dernier amour de l\'homme.\r\n\r\n', 'sahar', '2020-03-26'),
(19, 6, 'Il ne faut avoir aucun regret pour le passé, aucun remords pour le présent, et une confiance inébranlable pour l\'avenir.\r\n\r\n', 'hazem', '2020-02-06'),
(20, 4, 'Agis avec gentillesse, mais n\'attends pas de la reconnaissance.\r\n\r\n', 'nesrine', '2020-03-19'),
(21, 3, 'Une femme qu\'on aime est toute une famille.\r\n\r\n', 'amne', '2018-06-13'),
(22, 6, 'Il n\'existe que deux choses infinies, l\'univers et la bêtise humaine... mais pour l\'univers, je n\'ai pas de certitude absolue.\r\n\r\n', 'hazem', '2020-01-09'),
(23, 3, 'Le bonheur c\'est lorsque vos actes sont en accord avec vos paroles.\r\n\r\n', 'mouna', '2019-11-11'),
(24, 4, 'Lorsque l\'on se cogne la tête contre un pot et que cela sonne creux, ça n\'est pas forcément le pot qui est vide.\r\n\r\n', 'amine', '2019-12-07');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
