<?php

    function getArticles($a=null)
    {
        // Connexion à la base de données
        try
        {
            $bdd = new PDO('mysql:host=localhost;dbname=database;charset=utf8', 'root', '');
        }
        catch(Exception $e)
        {
                die('Erreur : '.$e->getMessage());
        }
        
        $compare = $bdd->query('SELECT * FROM articles ORDER BY id DESC');
        $i = $compare->fetch();

        if (($a>htmlspecialchars($i['id'])) || ($a==null))
        {
            $compare->closeCursor();

            $article = $bdd->query('SELECT * FROM articles WHERE (publication_date < current_date) ORDER BY publication_date DESC');

            // Affichage de chaque message (toutes les données sont protégées par htmlspecialchars)
            while ($donnees = $article->fetch())
            {
                echo '<p><strong>nom de l\'article</strong> : ' . htmlspecialchars($donnees['title']) . '</p>';
                echo '<p><strong>contenu</strong> : <em>' . htmlspecialchars($donnees['texte']) . '</em></p>';
                echo '<p><strong>auteur </strong> : ' . htmlspecialchars($donnees['author']) . '</p>';
                echo '<p><strong>date de publication</strong> : ' . htmlspecialchars($donnees['publication_date']) . '</p>';
                ?>
                <p>(<a href="delete.php?id=<?php echo htmlspecialchars($donnees['id']); ?>">Supprimer</a>)</p>
                <p>(<a href="commentaires.php?id=<?php echo htmlspecialchars($donnees['id']); ?>">Commentaires</a>)</p>
                <?php
                echo '<br />';
            }
            $article->closeCursor();
        }
        else
        {
            $compare->closeCursor();
            // Récupération des articles
            $article = $bdd->query('SELECT * FROM articles WHERE (publication_date <= current_date) ORDER BY publication_date DESC');

            // Affichage de chaque message (toutes les données sont protégées par htmlspecialchars)
            while (($donnees = $article->fetch()) && ($a>0) )
            {
                echo '<p><strong>nom de l\'article</strong> : ' . htmlspecialchars($donnees['title']) . '</p>';
                echo '<p><strong>contenu</strong> : <em>' . htmlspecialchars($donnees['texte']) . '</em></p>';
                echo '<p><strong>auteur </strong> : ' . htmlspecialchars($donnees['author']) . '</p>';
                echo '<p><strong>date de publication</strong> : ' . htmlspecialchars($donnees['publication_date']) . '</p>';
                ?>
                <p>(<a href="delete.php?id=<?php echo htmlspecialchars($donnees['id']); ?>">Supprimer</a>)</p>
                <p>(<a href="commentaires.php?id=<?php echo htmlspecialchars($donnees['id']); ?>">Commentaires</a>)</p>
                <?php
                echo '<br />'; 
                $a--;
            }
            $article->closeCursor();
        }
    }

?>