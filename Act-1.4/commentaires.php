<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Mon Artcles=</title>
    </head>
        
    <body>

        <h1>Mon super article !</h1>
        <p><a href="articles.php">Retour à la liste des articles</a></p>
 
        <?php

        if(isset ($_GET['id']))
        {
            // Connexion à la base de données
            try
            {
                $bdd = new PDO('mysql:host=localhost;dbname=database;charset=utf8', 'root', '');
            }
            catch(Exception $e)
            {
                    die('Erreur : '.$e->getMessage());
            }

            $id = $_GET['id'];
            
            // Récupération du billet
            $req = $bdd->prepare('SELECT * FROM articles WHERE id = ?');
            $req->execute(array($id));
            $donnees = $req->fetch();

            echo '<p><strong>nom de l\'article</strong> : ' . htmlspecialchars($donnees['title']) . '</p>';
            echo '<p><strong>contenu</strong> : <em>' . htmlspecialchars($donnees['texte']) . '</em></p>';
            echo '<p><strong>auteur </strong> : ' . htmlspecialchars($donnees['author']) . '</p>';
            echo '<p><strong>date de publication</strong> : ' . htmlspecialchars($donnees['publication_date']) . '</p>';
            ?>

            <h2>Commentaires</h2>

            <?php
            $req->closeCursor(); // Important : on libère le curseur pour la prochaine requête

            // Récupération des commentaires
            $req = $bdd->prepare('SELECT * FROM commentaires WHERE article_id = ? ORDER BY comment_date DESC');
            $req->execute(array($id));

            while ($donnees = $req->fetch())
            {
            ?>
            <p><strong><?php echo htmlspecialchars($donnees['author']); ?></strong> (<?php echo $donnees['comment_date']; ?>)</p>
            <p><?php echo nl2br(htmlspecialchars($donnees['texte'])); ?></p>
            <?php
            } // Fin de la boucle des commentaires
            $req->closeCursor();
            ?>
            
            <form action="ajout_commentaire.php?id=<?php echo $id; ?>" method="post">
                <p>
                    <label for="author"><strong>Votre Pseudo</strong></label><br /><input type="text" name="author" id="author" /><br />
                    <label for="texte"></label><br /><textarea name="texte" placeholder="commenter..." rows="8" cols="45"></textarea><br />
                    <input type="submit" value="Ajouter un commentaire" />
                </p>
            </form>
            
            <?php
        }
        ?>

    </body>
</html>