<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Poster un article</title>
    </head>
    </head>
    <style>
    form
    {
        text-align:center;
    }
    </style>
    <body>

        <!-- L'en-tête -->
    
        <?php include("header.php"); ?>

        <!-- Le corps -->

        <form action="target.php" method="post">
            <p>
                <label for="title">title</label><br /><input type="text" name="title" id="title" /><br />

                <label for="texte">text</label><br /><textarea name="texte" placeholder="contenu de l'article" rows="8" cols="45"></textarea><br />

                <label for="author">author</label><br /><input type="text" name="author" id="author" /><br />

                <label for="publication_date">publication_date</label><br /><input type="date" name="publication_date" id="publication_date" /><br />
                
                <input type="submit" value="Publier" />
            </p>
    </form>