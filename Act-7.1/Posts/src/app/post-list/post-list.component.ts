import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';

import {  Post } from '../post';
import { PostListItemComponent } from '../post-list-item/post-list-item.component';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {

  @ViewChildren('title') childrenComponent: QueryList<PostListItemComponent>;

  Post1 = new Post('Mon premier post','A new study uses machine learning to project migration patterns resulting from sea-level rise. Researchers found the impact of rising oceans will ripple across the country, beyond coastal areas at risk of flooding, as affected people move inland. Popular relocation choices will include land-locked cities such as Atlanta, Houston, Dallas, Denver and Las Vegas. The model also predicts suburban and rural areas in the Midwest will experience disproportionately large influx of people relative to their smaller local populations.',3,new Date("2017-10-24"));
  Post2 = new Post('Mon deuxième post','Scientists have discovered Earth\'s oldest asteroid strike occurred at Yarrabubba, in outback Western Australia, and coincided with the end of a global deep freeze known as a Snowball Earth. The research used isotopic analysis of minerals to calculate the precise age of the Yarrabubba crater for the first time, putting it at 2.229 billion years old -- making it 200 million years older than the next oldest impact',-1,new Date("2017-10-24"));
  Post3 = new Post('Encore un post','Scientists have found evidence to support long-standing anecdotes that stress causes hair graying. Researchers found that in mice, the type of nerve involved in the fight-or-flight response causes permanent damage to the pigment-regenerating stem cells in the hair follicle. The findings advance knowledge of how stress impacts the body, and are a first step toward blocking its negative effects',0,new Date("2017-10-24"));

  Posts = [this.Post1, this.Post2, this.Post3];

  constructor() { }

  ngOnInit() {
  }

}
