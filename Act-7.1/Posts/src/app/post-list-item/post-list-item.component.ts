import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core'

import {  Post } from '../post';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})

export class PostListItemComponent implements OnInit {

  @Input() Post: Post;

  constructor() { 
  }

  loveit() {
    this.Post.loveIts++;
  }

  dontloveit() {
    this.Post.loveIts--;
  }

  ngOnInit() {
  }

}
