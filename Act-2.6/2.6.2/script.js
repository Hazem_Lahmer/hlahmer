var nouvelleCourse = document.getElementById("course");
var bt = document.getElementById('submit');

// Affichage d'un message contextuel pour la saisie du pseudo
nouvelleCourse.addEventListener("focus", function () {
    document.getElementById("aideCourse").textContent = "Entrez le nom de la course";
});
// Suppression du message contextuel pour la saisie du pseudo
nouvelleCourse.addEventListener("blur", function (e) {
    document.getElementById("aideCourse").textContent = "";
});

// Affiche de toutes les données saisies
bt.addEventListener("click", function (e) {
    var course = nouvelleCourse.value;
    var noeuds = document.getElementById('courses');
    var nouvelNoeud = document.createElement('li');
    var textNoeud = document.createTextNode(course);

    nouvelNoeud.appendChild(textNoeud);
    noeuds.appendChild(nouvelNoeud);
    nouvelleCourse.value = "";
    e.preventDefault(); // Annulation de l'envoi des données
});