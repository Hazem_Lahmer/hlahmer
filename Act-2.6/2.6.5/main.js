// Fonction de désactivation de l'affichage des "tooltips"
function deactivateTooltips() {

    var tooltips = document.querySelectorAll('.tooltip'),
        tooltipsLength = tooltips.length;

    for (var i = 0; i < tooltipsLength; i++) {
        tooltips[i].style.display = 'none';
    }

}


// La fonction ci-dessous permet de récupérer la "tooltip" qui correspond à notre input

function getTooltip(elements) {

    while (elements = elements.nextSibling) {
        if (elements.className === 'tooltip') {
            return elements;
        }
    }

    return false;

}

var check = {}; // On met toutes nos fonctions dans un objet littéral

check['nom'] = function(id) {

    var nom = document.getElementById(id),
        tooltipStyle = getTooltip(nom).style;

    if (nom.value.length < 20 && nom.value.length > 0) {
        nom.className = 'correct';
        tooltipStyle.display = 'none';
        return true;
    } else {
        nom.className = 'incorrect';
        tooltipStyle.display = 'inline-block';
        return false;
    }

};

check['prenom'] = function(id) {

    var prenom = document.getElementById(id),
        tooltipStyle = getTooltip(prenom).style;

    if (prenom.value.length < 15 && prenom.value.length > 0) {
        prenom.className = 'correct';
        tooltipStyle.display = 'none';
        return true;
    } else {
        prenom.className = 'incorrect';
        tooltipStyle.display = 'inline-block';
        return false;
    }

};

check['numero'] = function(id) {

    var numero = document.getElementById(id),
        tooltipStyle = getTooltip(numero).style;

    if (numero.value.length == 10) {
        numero.className = 'correct';
        tooltipStyle.display = 'none';
        return true;
    } else {
        numero.className = 'incorrect';
        tooltipStyle.display = 'inline-block';
        return false;
    }

};


// Mise en place des événements

(function() { // Utilisation d'une IIFE pour éviter les variables globales.

    var myForm = document.getElementById('myForm'),
        inputs = document.querySelectorAll('input[type=text]'),
        inputsLength = inputs.length;

    for (var i = 0; i < inputsLength; i++) {
        inputs[i].addEventListener('keyup', function(e) {
            check[e.target.id](e.target.id); // "e.target" représente l'input actuellement modifié
        });
    }

    myForm.addEventListener('submit', function(e) {

        var result = true;

        for (var i in check) {
            result = check[i](i) && result;
        }

        if (result) {
            alert('Le formulaire est bien rempli.');
        }

        e.preventDefault();

    });

    myForm.addEventListener('reset', function() {

        for (var i = 0; i < inputsLength; i++) {
            inputs[i].className = '';
        }

        deactivateTooltips();

    });

})();


// Maintenant que tout est initialisé, on peut désactiver les "tooltips"

deactivateTooltips();