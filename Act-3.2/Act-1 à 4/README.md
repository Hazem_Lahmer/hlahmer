design patterns:
----------------

activité 1

on a une page qui contient tous ce qu'il faut pour afficher les articles récupérés de la BDD et un formulaire pour ajouter un article

activité 2

séparation du code entre les fichier Article.php contenant les requetes SQL et le fichier article.php qui contient le HTML

activité 3

création d'un fichier controlleur (controler.php) faisant le lien entre le modele et le view

activité 4

ajout de la classe Connexion.php qui utilise le singleton pour récupérer tous les posts de la BDD

