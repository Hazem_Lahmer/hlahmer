<?php

require_once ('index.php');
require_once ('Connexion.php');

class Article
{
    protected $title, $texte, $author, $bdd ;

    // Connexion à la base de données

    public function __construct ()
    {
        $this->bdd = Connexion::getInstance();
    }

    public function recuperer ()
    {
        $post = $this->bdd->query('SELECT * FROM posts ORDER BY publication_date DESC');
        return $post;
    }

    // Insertion
    public function inserer ($title, $texte, $author)
    {
        $req = $this->bdd->prepare('INSERT INTO posts(title, texte, author, publication_date) VALUES(:title, :texte, :author, current_date ())');
        $req->execute(array(
            'title' => $title,
            'texte' => $texte,
            'author' => $author));
    }

}