<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title>HOME PAGE</title>
    <link href="style.css" rel="stylesheet" />
</head>
<body>

<!-- affichage des posts -->

    <h1>posts</h1>
    <?php
        // Affichage de chaque message (toutes les données sont protégées par htmlspecialchars)
        while ($donnees = $post->fetch())
        {
            ?>
            <div class="news">
                <h3>
                    <?php echo htmlspecialchars($donnees['title']); ?>
                    <em>le <?php echo $donnees['publication_date']; ?></em>
                </h3>

                <p>
                    <?php
                    // On affiche le contenu du billet
                    echo nl2br(htmlspecialchars($donnees['texte']));
                    ?>
                    <br />
                    <em><?php echo $donnees['author']; ?></em>
                </p>
            </div>
            <?php
            echo '<br />';
        }
        $post->closeCursor();
    ?>

    <!-- formulaire d'ajout des posts -->

    <form action="" method="post">
        <p>
            <label for="title"><strong>title</strong></label><br /><input type="text" name="title" id="title" /><br />

            <label for="texte"><strong>text</strong></label><br /><textarea name="texte" placeholder="contenu de l'article" rows="8" cols="45"></textarea><br />

            <label for="author"><strong>author</strong></label><br /><input type="text" name="author" id="author" /><br />

            <input type="submit" value="Publier" />
        </p>
    </form>

</body>
</html>

