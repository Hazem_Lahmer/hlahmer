<?php


class Connexion
{
    protected static $db, $instance; // Contiendra l'instance de notre classe.

    // Constructeur en privé.
    protected function __construct()
    {
        try
        {
            self::$db = new PDO('mysql:host=localhost;dbname=blog;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        }
        catch(Exception $e)
        {
            die('Erreur : '.$e->getMessage());
        }
    }

    // Méthode de clonage en privé aussi.
    protected function __clone() { }

    public static function getInstance()
    {
        if (!isset(self::$instance)) // Si on n'a pas encore instancié notre classe.
        {
            self::$instance = new self; // On s'instancie nous-mêmes. :)
        }

        return self::$db;
    }

}