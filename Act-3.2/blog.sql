-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 26 déc. 2019 à 07:55
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `blog`
--

-- --------------------------------------------------------

--
-- Structure de la table `commentaires`
--

DROP TABLE IF EXISTS `commentaires`;
CREATE TABLE IF NOT EXISTS `commentaires` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `texte` text NOT NULL,
  `author` varchar(255) NOT NULL,
  `comment_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `commentaires`
--

INSERT INTO `commentaires` (`id`, `article_id`, `texte`, `author`, `comment_date`) VALUES
(1, 1, 'La vie c\'est des étapes... La plus douce c\'est l\'amour... La plus dure c\'est la séparation... La plus pénible c\'est les adieux... La plus belle c\'est les retrouvailles.', 'sahar', '2019-09-11'),
(2, 1, 'Dans la vie on ne fait pas ce que l\'on veut mais on est responsable de ce que l\'on est.', 'wahib', '2019-08-14'),
(3, 1, 'La vie est un mystère qu\'il faut vivre, et non un problème à résoudre.', 'ibtissem', '2019-08-14'),
(4, 1, 'L\'une des plus grandes douleurs est d\'aimer une personne que tu ne peux pas avoir.\r\n\r\n', 'marouane', '2019-12-09'),
(5, 2, 'On passe une moitié de sa vie à attendre ceux qu\'on aimera et l\'autre moitié à quitter ceux qu\'on aime.\r\n\r\n', 'nesrine', '2019-09-24'),
(6, 2, 'La vie, c\'est comme une bicyclette, il faut avancer pour ne pas perdre l\'équilibre.', 'islem', '2019-01-21'),
(7, 2, 'Pour critiquer les gens il faut les connaître, et pour les connaître, il faut les aimer.\r\n\r\n', 'hazem', '2019-12-01'),
(8, 2, 'La règle d\'or de la conduite est la tolérance mutuelle, car nous ne penserons jamais tous de la même façon, nous ne verrons qu\'une partie de la vérité et sous des angles différents.\r\n\r\n', 'amine', '2018-11-14'),
(9, 5, 'Choisissez un travail que vous aimez et vous n\'aurez pas à travailler un seul jour de votre vie.\r\n\r\n', 'mouna', '2019-07-15'),
(10, 6, 'Le monde est dangereux à vivre ! Non pas tant à cause de ceux qui font le mal, mais à cause de ceux qui regardent et laissent faire.\r\n\r\n', 'mouna', '2018-09-12'),
(11, 4, 'Il est difficile de dire adieu lorsqu\'on veut rester, compliqué de rire lorsqu\'on veut pleurer, mais le plus terrible est de devoir oublier lorsqu\'on veut aimer.\r\n\r\n', 'ibtissem', '2019-12-30'),
(12, 3, 'On ne souffre jamais que du mal que nous font ceux qu\'on aime. Le mal qui vient d\'un ennemi ne compte pas.\r\n\r\n', 'hazem', '2019-11-20'),
(13, 5, 'Le sentiment de ne pas être aimé est la plus grande des pauvretés.\r\n\r\n', 'sahar', '2019-12-28'),
(14, 5, 'Dans la vengeance et en amour, la femme est plus barbare que l\'homme.\r\n\r\n', 'marouane', '2019-08-29'),
(15, 5, 'Un seul être vous manque et tout est dépeuplé.\r\n\r\n', 'wahib', '2018-12-13'),
(16, 3, 'Aimer, ce n\'est pas se regarder l\'un l\'autre, c\'est regarder ensemble dans la même direction.\r\n\r\n', 'wahib', '2019-04-07'),
(17, 4, 'La nature fait les hommes semblables, la vie les rend différents.\r\n\r\n', 'islem', '2019-05-06'),
(18, 6, 'L\'homme veut être le premier amour de la femme, alors que la femme veut être le dernier amour de l\'homme.\r\n\r\n', 'sahar', '2020-03-26'),
(19, 6, 'Il ne faut avoir aucun regret pour le passé, aucun remords pour le présent, et une confiance inébranlable pour l\'avenir.\r\n\r\n', 'hazem', '2020-02-06'),
(20, 4, 'Agis avec gentillesse, mais n\'attends pas de la reconnaissance.\r\n\r\n', 'nesrine', '2020-03-19'),
(21, 3, 'Une femme qu\'on aime est toute une famille.\r\n\r\n', 'amne', '2018-06-13'),
(22, 6, 'Il n\'existe que deux choses infinies, l\'univers et la bêtise humaine... mais pour l\'univers, je n\'ai pas de certitude absolue.\r\n\r\n', 'hazem', '2020-01-09'),
(23, 3, 'Le bonheur c\'est lorsque vos actes sont en accord avec vos paroles.\r\n\r\n', 'mouna', '2019-11-11'),
(24, 4, 'Lorsque l\'on se cogne la tête contre un pot et que cela sonne creux, ça n\'est pas forcément le pot qui est vide.\r\n\r\n', 'amine', '2019-12-07'),
(25, 5, 'bla bla bla', 'hazem', '2019-12-06');

-- --------------------------------------------------------

--
-- Structure de la table `membres`
--

DROP TABLE IF EXISTS `membres`;
CREATE TABLE IF NOT EXISTS `membres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `inscription_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `texte` text NOT NULL,
  `author` varchar(255) NOT NULL,
  `publication_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `posts`
--

INSERT INTO `posts` (`id`, `title`, `texte`, `author`, `publication_date`) VALUES
(1, 'Trump « honoré » d\'avoir joué dans « Maman, j\'ai encore raté l’avion »', 'Donald Trump n’y apparaît que très succinctement. Pour lui, c’est tout de même un « honneur ». Bien avant de s’installer à la Maison-Blanche, le futur président des Etats-Unis, alors encore simple homme d’affaires, a joué les figurants dans le film de Noël Maman, j’ai encore raté l’avion.\r\n\r\nC’est un soldat qui a mis mardi le sujet sur la table (de Noël !) lors d’un échange entre le président et des troupes américaines déployées à travers le monde. « J’étais un peu plus jeune, on va dire », a raconté l’ancien magnat de l’immobilier, qui venait d’acquérir le Plaza Hotel de New York lorsque plusieurs scènes du film y ont été tournées au début des années 1990. « C’est évidemment devenu un grand succès », a-t-il poursuivi. « Un grand succès de Noël, l’un des plus grands. C’est un honneur d’avoir été impliqué dans une chose pareille. »', 'Stevan Harnad', '2019-12-04'),
(2, 'Allemagne : Que sait-on de l’enfant disparu depuis deux ans, retrouvé dans un placard chez un pédophile ?', 'C\'est un simple article aussi\r\nrien de spécial', 'Thomas Samuel Kuhn', '2019-11-14'),
(4, 'Loire : Deux enfants se noient dans une rivière', 'Deux jeunes enfants sont morts noyés dans une rivière au bord de laquelle ils jouaient en présence de leur mère, mardi en fin de journée à Montbrison (Loire), a-t-on appris auprès de la gendarmerie et de la préfecture du département. Les deux victimes, frère et sœur, sont nés en 2014 et 2017. Le drame est survenu dans Le Moingt, dont les eaux ont été gonflées dernièrement par d’abondantes pluies.\r\n\r\nLa mère, âgée de 35 ans et originaire du Kosovo, avait mangé avec les enfants dans un fast-food, avant de les emmener en fin d’après-midi jouer au bord de la rivière qui passe non loin de la zone commerciale de Montbrison. D’après ses déclarations, le plus jeune est tombé à l’eau. Sa sœur a essayé de le rattraper avant d’être emportée à son tour par le courant. La mère a réussi à les rejoindre mais sans pouvoir les ranimer. Elle a alors alerté des personnes pour qu’ils préviennent les secours. Arrivés sur place, ceux-ci n’ont rien pu faire, selon les mêmes sources.\r\n\r\nLa mère, sous le choc, a été hospitalisée. Le parquet de Saint-Étienne a ouvert une enquête, confiée à la brigade de recherches de Montbrison, et ordonné l’autopsie des deux corps.', 'Robert Sternberg', '2019-12-24'),
(5, '«Star Wars IX» : La confidence que Finn voulait faire à Rey révélée par J.J. Abrams', 'Le dernier volet de la troisième trilogie Star Wars vient de sortir en salle. Et, comme d’habitude avec cette trilogie, l’épisode IX divise fortement les fans. S’il a répondu à de nombreuses interrogations, il en a créé de nouvelles, notamment sur ce que voulait dire Finn à Rey.\r\nÀ plusieurs reprises, l’ex-stromstrooper a essayé de parler seul à seul avec Rey pour lui révéler, semble-t-il, une chose importante. Si certains ont pu croire que Finn voulait lui dévoiler ses sentiments, il n’en est rien.\r\n\r\nLors d’une projection privée en présence de J.J. Abrams, une spectatrice présente a rapporté les propos tenus par le réalisateur lors d’une discussion avec des fans. L’un d’entre eux lui a demandé des précisions sur la révélation que Finn voulait faire à Rey. Celui-ci lui aurait indiqué que Finn voulait avouer à la jeune femme qu’il était sensible à la Force.', 'Jan Hendrik Schön', '2019-09-19'),
(6, 'Grèves : Un syndicat CGT remet un chèque de 250.000 euros aux salariés de la RATP\r\n', 'Le syndicat Info’Com-CGT a remis mardi un chèque de 250.000 euros issus de sa caisse de grève aux salariés de la RATP, au 20e jour de la grève dans les transports contre la réforme des retraites, a constaté un journaliste de l’AFP.\r\n\r\n« C’est le soutien concret d’une grande partie de la population. On reçoit des chèques de partout en France, des salariés du privé, des enseignants, des retraités », a expliqué à l’AFP Marianne Ravaud, secrétaire générale adjointe du syndicat CGT des salariés de l’information et de la communication. « On a des petits dons d’une dizaine d’euros. Des gros, de plusieurs centaines… », a-t-elle poursuivi, en marge de la remise de ce chèque devant le siège de la RATP à Paris.', 'Loet Leydesdorff', '2019-07-04'),
(7, '« 13 Reasons Why », « Esprits criminels », « Vikings »... Quelles sont les séries télé américaines qui se terminent en 2020 ?', '2020, c’est une nouvelle décennie, des cérémonies de remises de prix, des nouveaux films et des nouvelles séries attendues… Mais aussi plusieurs séries populaires qui s’arrêtent pour de bon – jusqu’à ce qu’Hollywood décide de les rebooter sur une plateforme de streaming.\r\n\r\n\r\n', 'Alan Sokal', '2020-01-01');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
