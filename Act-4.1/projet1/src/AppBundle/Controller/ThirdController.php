<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

// ...
class ThirdController extends Controller
{
    /**
     * @Route("/errorpage/{code}", name="article_route")
     */
    public function erreur($code)
    {
        if (preg_match('/^4\d{2}$/', $code))
        {
            if ($code == '404') 
            {
                return $this->render('hazem/404.html.twig', [
                    'code' => $code,
                ]);
            } 
            else
            {
                return $this->render('hazem/4xxerror.html.twig', [
                    'code' => $code,
                ]);
            } 
        }
    }
}
