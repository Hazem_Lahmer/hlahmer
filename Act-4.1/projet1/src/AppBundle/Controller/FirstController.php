<?php
// src/AppBundle/Controller/LuckyController.php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

// ...
class FirstController extends Controller
{
    /**
     * @Route("/firstpage")
     */
    public function personnes()
    {
        $hazem = 'je m\'appelle hazem';
        $sahar = 'je m\'appelle sahar';

        return $this->render('hazem/page1.html.twig', [
            'hazem' => $hazem,
            'sahar' => $sahar,
        ]);
    }
    
        /**
     * @Route("/secondpage")
     */
    public function animaux()
    {
        $chien = 'c\'est un chien';
        $chat = 'c\'est un chat';
        $souris = 'c\'est une souris';

        return $this->render('hazem/page2.html.twig', [
            'chien' => $chien,
            'chat' => $chat,
            'souris' => $souris,
        ]);
    }

}