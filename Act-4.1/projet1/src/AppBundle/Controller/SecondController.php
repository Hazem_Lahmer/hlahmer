<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

// ...
class SecondController extends Controller
{
    /**
     * @Route("/article/{id}", name="article_route")
     */
    public function article($id)
    {
        if (preg_match('/^\d+$/', $id))
        {
            if ($id < 3) 
            {
                return $this->render('hazem/page3.html.twig');
            } 
            elseif ($id > 3) 
            {
                return $this->render('hazem/page4.html.twig');
            } 
            else 
            {
                return $this->render('hazem/page5.html.twig', [
                    'id' => $id,
                ]);
            }
        }
    }
}
