activité 2 : 

la page 1 : "http://127.0.0.1:8000/firstpage"

la page 2 : "http://127.0.0.1:8000/secondpage"

template : la page contenant le header et le footer et pour le corps il sera rempli dans chaque page différement

--------------------------------------------------------------------------------------------------------------

activité 3 :

vous pouvez voir le SecondController "http://127.0.0.1:8000/article/id" qui nous dirige vers page3.html.twig ou page4.html.twig ou page5.html.twig selon la valeur de l'id

si l'id contient d'autres caractères non numérique il n'aura accès à aucune page

vous pouvez mettre à la place de "id" la valeur que vous souhaité pour tester

--------------------------------------------------------------------------------------------------------------

activité 4 :

vous pouvez voir le ThirdController "http://127.0.0.1:8000/errorpage/code" qui nous dirige vers 404.html.twig ou 4xx.html.twig selon la valeur du code

vous pouvez mettre à la place de "code" la valeur que vous souhaité pour tester il prend en considération que les valeurs entre 400 et 499